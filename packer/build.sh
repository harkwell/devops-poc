#!/bin/bash

D=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
ISOFILE=${1:-/tmp/CentOS-7-x86_64-Minimal.iso}
SHCMDS="
	packer qemu-system-x86_64 qemu-img
"

###################
### SANITYCHECK ###
###################
for cmd in $SHCMDS; do
	which $cmd 2>&1 >/dev/null || { echo "missing: $cmd"; exit 1; }
done
for fl in $D/centos7.json $ISOFILE; do
	[ -f $fl ] || { echo "missing: $fl"; exit 1; }
done

############
### MAIN ###
############
echo "running packer, please wait (this could take a while)..."
export PACKER_LOG=1
/usr/local/packer/bin/packer build \
	-var "isofile=$ISOFILE" -var "hddsize=200000" \
	-var "md5sum=$(md5sum $ISOFILE |cut -d\  -f1)" $D/centos7.json
