Build A Virtual Machine Image with packer
=================
BUILD
---------------
```shell
## docker (or use centos7)  -docker seems to fail here
git clone https://gitlab.com/harkwell/devops-poc.git && cd devops-poc/packer
#docker run -it --privileged --name packer -h packer -v $PWD:/tmp centos

# download dependencies
yum install -y epel-release
yum install -y which wget bsdtar qemu-system-x86 qemu-img qemu-kvm
mkdir -p /usr/local/packer/bin/ && cd /usr/local/packer/bin/
wget -c 'https://releases.hashicorp.com/packer/1.2.4/packer_1.2.4_linux_amd64.zip' -O - | bsdtar -xvf -
chmod 755 /usr/local/packer/bin/packer
# make sure no VM is running (qemu runs into resource virtualization contention)
cd -
bash build.sh
mv images/centos7-khallware centos7.img
qemu-img convert -f raw -O vmdk centos7.img centos7.vmdk
chmod 777 centos7.vmdk
exit
docker rm packer

yum install -y qemu-system-x86_64
qemu-system-x86_64 centos7.vmdk
```

One-Time-Only
---------------
```shell
chromium-browser https://wiki.centos.org/Download
ls -ld CentOS-7-x86_64-Minimal.iso
```
