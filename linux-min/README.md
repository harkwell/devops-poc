Build A Virtual Machine Image with linux
=================
BUILD
---------------
```shell
# docker (or use centos7)
git clone https://gitlab.com/harkwell/devops-poc && cd devops-poc/linux-min
docker run -it --privileged --name linux-min -h linux-min -v $PWD:/tmp centos

# download dependencies
yum install -y which wget lvm2 e2fsprogs qemu-kvm grub2-pc-modules
lsmod | grep kvm
bash /tmp/build.sh
ls -ld /tmp/images/linux.vmdk
exit
docker rm linux-min

# no workie.....
yum install -y vmplayer
vmplayer $PWD/images/linux.vmdk
# OR
yum install -y qemu-system-x86_64
qemu-system-i386 $PWD/images/linux.vmdk
qemu-system-x86_64 $PWD/images/linux.vmdk
qemu-system-x86_64 -drive file=$PWD/images/hdd.img,if=ide -m 512
```

BOOT-UP
---------------
* POST

When most computers are powered on, the motherboard loads the BIOS into RAM
and the program counter is set to the correct offset and machine language
instructions are executed that check the health of the system.  This is
commonly referred to as Power-on-Self-Test (or POST).

* MBR

The last thing that the BIOS firmware does after it completes the POST is to
execute the master boot record (MBR).   The MBR constitutes the first 512 bytes
of the master disk drive as defined in the BIOS.  The MBR machine language is
loaded into RAM, the program counter set and instructions executed that
discover the active boot partition of the partition table (stored as the
next 64 bytes after the MBR).

* GRUB

The MBR hands execution off to the GRand Unified Bootloader (GRUB).  GRUB
stage one is part of the MBR itself. GRUB stage two is located in the
linux /boot filesystem (usually a separate disk partition whose offset is
stored in the MBR by GRUB).  GRUB stage one's job is to load stage two.
GRUB stage two reads its configuration file from /boot and the linux kernel
and the initial ramdisk (INITRD) are uncompressed and stored in memory.  The
kernel is executed and the INITRD is written to the ramdisk filesystem.

* INITRD

Kernel modules and the linuxrc live on the INITRD.  The linux kernel executes
the linuxrc where the real filesystem is mounted and a pivot_root replaces the
INITRD with that.

* INIT

The /sbin/init program is executed and assigned a process identifier of 1.
All other programs descend from this ancestor program.  INIT then runs the
systemd or RC scripts to take the system into the provisioned run level.

* BASH

The kernel attaches terminals and runs shell daemons that allow users access
to the system.  The BASH shell program is typically the one assigned to
user accounts and is the primary way they interact with the system.

* X11 Window System

Another program may be executed that provides a graphical user interface for
local users.  Elaborate window and session managers exist such as gnome that
provide a rich user experience.
