#!/bin/bash

D=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
DESTDIR=${1:-/tmp/images}
GIGHDDSIZE=${2:-15}
KERNELVER=3.10.0-862
URL=http://mirror.centos.org/centos/7/os/x86_64/Packages
LOOPDEV=/dev/loop0
CHROOTDIR=/tmp/chroot
DEPS="
   systemd-219-57.el7 bash-4.2.46-30.el7
   glibc-2.17-222.el7 ncurses-libs-5.9-14.20130511.el7_4
   coreutils-8.22-21.el7 libselinux-2.5-12.el7 libcap-2.22-9.el7
   libacl-2.2.51-14.el7 pcre-8.32-17.el7 libattr-2.4.46-13.el7
   util-linux-2.23.2-52.el7 findutils-4.5.11-5.el7 gzip-1.5-10.el7
   cpio-2.11-27.el7 libgcc-4.8.5-28.el7 grep-2.20-3.el7 sed-4.2.2-5.el7
   libblkid-2.23.2-52.el7 pam-1.1.8-22.el7 kmod-20-21.el7 xz-libs-5.2.2-1.el7
   zlib-1.2.7-17.el7 xz-5.2.2-1.el7 libuuid-2.23.2-52.el7
   audit-libs-2.8.1-3.el7 kmod-libs-20-21.el7 libmount-2.23.2-52.el7
   libcap-ng-0.7.5-4.el7 device-mapper-libs-1.02.146-4.el7
   libsepol-2.5-8.1.el7 systemd-libs-219-57.el7 elfutils-libs-0.170-4.el7
   elfutils-libelf-0.170-4.el7 bzip2-libs-1.0.6-13.el7
"

#######################
### TROUBLESHOOTING ###
#######################
### if losetup shows autoclear and it won't release the file descriptor
cat <<EOF >/dev/null
dmsetup ls
dmsetup remove vg00-lvol4
dmsetup remove vg00-lvol3
dmsetup remove vg00-lvol2
dmsetup remove vg00-lvol1
dmsetup remove vg00-swap
#losetup -D
losetup
EOF


###################
### SANITYCHECK ###
###################
BASHDEPS="wget mkfs.ext4 qemu-img pvcreate mkinitrd"

for cmd in $BASHDEPS; do
	which $cmd >/dev/null || { echo missing: $cmd; exit 1; }
done
[ $(id -u) -eq 0 ] || { echo "must be root!"; exit 1; }
[ -f $DESTDIR/hdd.img ] && { exit "image exists: $DESTDIR/hdd.img"; exit 1; }
[ -f /usr/lib/grub/i386-pc/boot.img ] || { exit "grub images missing"; exit 1; }

##################
### INITIALIZE ###
##################
mkdir -p $DESTDIR /tmp/vmdk-mnt

#################
### FUNCTIONS ###
#################
function setup_etc()
{
	echo "Setting up /etc/..."
	DIR=${1:-$CHROOTDIR}
	cp -pr /tmp/etc $DIR/
}

function setup_bin()
{
	echo "Setting up system binaries..."
	DIR=${1:-$CHROOTDIR}

	for rpm in $DEPS; do
		shortname=$(echo $rpm |sed 's#-[0-9].*$##')
		printf "\t${shortname}\n"
		[ -f /tmp/${shortname}.rpm ] || wget -c $URL/${rpm}.x86_64.rpm -O /tmp/${shortname}.rpm
		cd $DIR/ && rpm2cpio /tmp/${shortname}.rpm |cpio -idm
	done
}

function setup_grub()
{
	echo "Setting up GRUB..."
	DIR=${1:-$CHROOTDIR}
	mkdir -p $DIR/boot/grub/
	cp $D/menu.lst $DIR/boot/grub/
	cp $D/message $DIR/boot/
	[ -f /tmp/kernel.rpm ] || wget -c $URL/kernel-$KERNELVER.el7.x86_64.rpm -O /tmp/kernel.rpm
	cd $DIR/ && rpm2cpio /tmp/kernel.rpm |cpio -idm

	cd /usr/lib/grub/i386-pc
	dd if=boot.img of=$DESTDIR/hdd.img bs=512 count=1 seek=2048
	dd if=diskboot.img of=$DESTDIR/hdd.img bs=512 seek=2049

	[ -f /tmp/drakut.rpm ] || wget -c $URL/dracut-033-535.el7.x86_64.rpm -O /tmp/drakut.rpm
	cd $DIR/ && rpm2cpio /tmp/drakut.rpm |cpio -idm
	#grub2-install $DESTDIR/hdd.img
	mkdir -p bin var/tmp dev
	ln -s /usr/bin/bash bin/bash
	ln -s /usr/bin/bash bin/sh
	mknod -m 666 dev/null c 1 3
	mknod -m 666 dev/kmsg c 1 11
	# dracut
	chroot $DIR/ /usr/sbin/depmod 3.10.0-862.el7.x86_64
	chroot $DIR/ /usr/bin/mkinitrd /boot/initrd.img 3.10.0-862.el7.x86_64 # matches menu.lst
	#chroot $DIR/ grub2-install --target=i386-qemu
}

############
### MAIN ###
############
echo "Creating image of size $GIGHDDSIZE GB"
dd if=/dev/zero of=$DESTDIR/hdd.img count=1K bs=${GIGHDDSIZE}M >/dev/null

echo "Partitioning HDD image"
sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' <<-EOF |fdisk $DESTDIR/hdd.img >/dev/null
   o     # clear
   n     # new partition
   p     # make it primary
   1     # first partition (boot)
         # default first sector
   +500M # last sector (end of disk)
   n     # new partition
   p     # make it primary
   2     # second partition (LVM)
         # next sector
         # last sector (end of disk)
   t     # change type
   2     # second partition
   8e    # Linux LVM
   a     # make bootable
   1     # first partition
   p     # print
   w     # write
   q     # quit
EOF

# offset is units times the starting sector
echo "Formatting /boot filesystem"
losetup $LOOPDEV $DESTDIR/hdd.img -o $((512 * 2048)) || { echo "failed to setup loopback!"; exit 1; }
mkfs.ext4 $LOOPDEV 500m >/dev/null
mount $LOOPDEV /tmp/vmdk-mnt || { echo "mount failed!"; exit 1; }
umount /tmp/vmdk-mnt/
losetup -d $LOOPDEV >/dev/null

echo "Provisioning LVM disk"
losetup $LOOPDEV $DESTDIR/hdd.img -o $((512 * 1026048))
pvcreate $LOOPDEV
vgcreate vg00 $LOOPDEV
sed -i -e 's#udev_sync = 1#udev_sync = 0#g' /etc/lvm/lvm.conf
sed -i -e 's#udev_rules = 1#udev_rules = 0#g' /etc/lvm/lvm.conf
vgchange -a y vg00
lvcreate -L500M  -n swap vg00  # swap
lvcreate -L500M  -n lvol1 vg00 # /
lvcreate -L5000M -n lvol2 vg00 # /usr
lvcreate -L5000M -n lvol3 vg00 # /var
lvcreate -L500M  -n lvol4 vg00 # /tmp
mkfs.ext4 /dev/vg00/lvol1 >/dev/null
mkfs.ext4 /dev/vg00/lvol2 >/dev/null
mkfs.ext4 /dev/vg00/lvol3 >/dev/null
mkfs.ext4 /dev/vg00/lvol4 >/dev/null
e2fsck -y /dev/vg00/lvol1
e2fsck -y /dev/vg00/lvol2
e2fsck -y /dev/vg00/lvol3
e2fsck -y /dev/vg00/lvol4
mkdir -p $CHROOTDIR
mount /dev/vg00/lvol1 $CHROOTDIR
mkdir -p $CHROOTDIR/{usr,var,tmp}
mount /dev/vg00/lvol2 $CHROOTDIR/usr
mount /dev/vg00/lvol3 $CHROOTDIR/var
mount /dev/vg00/lvol4 $CHROOTDIR/tmp
setup_etc
setup_bin
setup_grub
cd $D/
umount $CHROOTDIR/tmp
umount $CHROOTDIR/var
umount $CHROOTDIR/usr
umount $CHROOTDIR
vgchange -a n vg00
losetup -d $LOOPDEV

echo "Convert disk image to vmdk"
# this seems to create a smaller, non-functioning vmdk for some reason
qemu-img convert -p -O vmdk $DESTDIR/hdd.img $DESTDIR/linux.vmdk
