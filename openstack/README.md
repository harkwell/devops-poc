Become a Cloud Provider with OpenStack
=================
PoC
---------------
```shell
### Single devstack example
VBoxManage list vms # choose a Virtual Box VM with centos7
BOX=$(VBoxManage list vms |tail -1 |cut -d\" -f2)

# create a snapshot
VBoxManage snapshot $BOX take 'devstack'
VBoxManage snapshot $BOX list

# start it up, connect, add stack user, install devstack
VBoxManage startvm $BOX --type headless
MACADDR=$(VBoxManage showvminfo $BOX |grep '^NIC 1:' |cut -d: -f3 |cut -d, -f1 |sed -e 's# ##g' -e 's#\(..\)#\1:#g' -e 's#:$##' |tr 'A-Z' 'a-z')
IPADDR=$(arp -na |grep $MACADDR |cut -d\( -f2 |cut -d\) -f1)
ssh $BOX
sudo bash -o vi
useradd -s /bin/bash -d /opt/stack -m stack
echo "stack ALL=(ALL) NOPASSWD: ALL" >>/etc/sudoers.d/stack
su - stack
git clone https://git.openstack.org/openstack-dev/devstack && cd devstack
cat <<'EOF' >local.conf
[[local|localrc]]
ADMIN_PASSWORD=a1s2d3f4
DATABASE_PASSWORD=$ADMIN_PASSWORD
RABBIT_PASSWORD=$ADMIN_PASSWORD
SERVICE_PASSWORD=$ADMIN_PASSWORD
EOF
./stack.sh

# wait fifteen minutes or more, then use it
chromium-browser http://$IPADDR/identity

# clean up
VBoxManage controlvm $BOX poweroff
VBoxManage snapshot $BOX restore 'devstack'
VBoxManage snapshot $BOX delete 'devstack'
```
