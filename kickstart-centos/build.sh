#!/bin/bash

D=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
DESTDIR=${1:-/tmp/centos}
GIGHDDSIZE=${2:-15}
ISOFILE=${3:-/tmp/CentOS-7-x86_64-Minimal.iso}
KSURL=${4:-http://localhost/ks.cfg}

BASHDEPS="wget mkfs.ext4 qemu-img qemu-system-x86_64 pvcreate"

for cmd in $BASHDEPS; do
	which $cmd >/dev/null || { echo missing: $cmd; exit 1; }
done
[ $(id -u) -eq 0 ] || { echo "must be root!"; exit 1; }
wget -c "$KSURL" -O /dev/null 2>/dev/null || { echo "cannot retrieve kickstart file: $KSURL"; exit 1; }

##################
### INITIALIZE ###
##################
#[ -f $ISOFILE ] || wget https://buildlogs.centos.org/rolling/7/isos/x86_64/CentOS-7-x86_64-Minimal.iso -O $ISOFILE
[ -f $ISOFILE ] || { echo "missing iso: $ISOFILE"; exit 1; }
mkdir -p $DESTDIR /tmp/vmdk-mnt

#################
### FUNCTIONS ###
#################
setup_fstab()
{
	DIR=${1:-/tmp/chroot}
	#cat <<EOF >$DIR/etc/fstab
	#EOF
}

#################
### BASE DISK ###
#################
[ ! -f $DESTDIR/hdd.img ] && {
   echo "creating image of size $GIGHDDSIZE GB"
   dd if=/dev/zero of=$DESTDIR/hdd.img count=1K bs=${GIGHDDSIZE}M
}
echo "partitioning hdd image"
sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' <<-EOF |fdisk $DESTDIR/hdd.img >/dev/null
   o     # clear
   n     # new partition
   p     # make it primary
   1     # first partition (boot)
         # default first sector
   +500M # last sector (end of disk)
   n     # new partition
   p     # make it primary
   2     # second partition (LVM)
         # next sector
         # last sector (end of disk)
   t     # change type
   2     # second partition
   8e    # Linux LVM
   a     # make bootable
   1     # first partition
   p     # print
   w     # write
   q     # quit
EOF
# offset is units times the starting sector
echo "formatting /boot filesystem"
losetup /dev/loop0 $DESTDIR/hdd.img -o $((512 * 2048)) || { echo "failed to setup loopback!"; exit 1; }
mkfs.ext4 /dev/loop0 500m
mount /dev/loop0 /tmp/vmdk-mnt || { echo "mount failed!"; exit 1; }
umount /tmp/vmdk-mnt/
losetup -d /dev/loop0

echo "provisioning LVM disk"
losetup /dev/loop0 $DESTDIR/hdd.img -o $((512 * 1026048))
pvcreate /dev/loop0
vgcreate vg00 /dev/loop0
sed -i -e 's#udev_sync = 1#udev_sync = 0#g' /etc/lvm/lvm.conf
sed -i -e 's#udev_rules = 1#udev_rules = 0#g' /etc/lvm/lvm.conf
vgchange -a y vg00
lvcreate -L500M  -n swap vg00  # swap
lvcreate -L500M  -n lvol1 vg00 # /
lvcreate -L5000M -n lvol2 vg00 # /usr
lvcreate -L5000M -n lvol3 vg00 # /var
lvcreate -L500M  -n lvol4 vg00 # /tmp
mkfs.ext4 /dev/vg00/lvol1
mkfs.ext4 /dev/vg00/lvol2
mkfs.ext4 /dev/vg00/lvol3
mkfs.ext4 /dev/vg00/lvol4
mkdir -p /tmp/chroot
mount /dev/vg00/lvol1 /tmp/chroot
mkdir -p /tmp/chroot/{usr,var,tmp}
mount /dev/vg00/lvol2 /tmp/chroot/usr
mount /dev/vg00/lvol3 /tmp/chroot/var
mount /dev/vg00/lvol4 /tmp/chroot/tmp
setup_fstab
umount /tmp/chroot/tmp
umount /tmp/chroot/var
umount /tmp/chroot/usr
umount /tmp/chroot
vgchange -a n vg00
losetup -d /dev/loop0

#echo "convert disk image to vmdk"
qemu-img convert -p -O vmdk $DESTDIR/hdd.img $DESTDIR/centos.vmdk

#################
### KICKSTART ###
#################
qemu-system-x86_64 -enable-kvm            \
     -smp 4                               \
     -cpu host                            \
     -m 1G                                \
     -cdrom $ISOFILE                      \
     -boot order=c,once=d                 \
     -net nic,netdev=net0                 \
     -netdev user,id=net0                 \
     -soundhw ac97                        \
     -vga std                             \
     -serial mon:stdio                    \
     -kernel /isolinux/vmlinuz            \
     -append linux ks=$KSURL              \
     -name "centos7" $DESTDIR/centos.vmdk \
|| {
   echo "===================================================================="
   echo "NOTE!: please type 'tab', then 'ks=$KSURL'"
   echo "===================================================================="
   qemu-system-x86_64 -enable-kvm            \
        -smp 4                               \
        -cpu host                            \
        -m 1G                                \
        -cdrom $ISOFILE                      \
        -boot order=c,once=d                 \
        -net nic,netdev=net0                 \
        -netdev user,id=net0                 \
        -soundhw ac97                        \
        -vga std                             \
        -serial mon:stdio                    \
        -name "centos7" $DESTDIR/centos.vmdk
}
