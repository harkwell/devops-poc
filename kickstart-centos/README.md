Build A Centos7 Virtual Machine Image via kickstart
=================
BUILD
---------------
```shell
sudo ./build.sh
ls -ld /tmp/centos/centos.vmdk
vmplayer /tmp/centos/centos.vmdk
qemu-system-x86_64 /tmp/centos/centos.vmdk

# docker
docker exec -it qemu bash
IPADDR=$(ip addr |grep 'inet ' |tail -1 |awk '{print $2}' |cut -d/ -f1)
bash /tmp/build.sh /tmp/centos 15 /tmp/CentOS-7-x86_64-Minimal.iso http://$IPADDR/ks.cfg
exit
ls -ld /tmp/qemu/centos/centos.vmdk
vmplayer /tmp/centos/centos.vmdk
qemu-system-x86_64 /tmp/centos/centos.vmdk
```

One-Time-Only
---------------
* Download the Centos7 Minimal ISO image
This contains the linux kernel binary and scripts that allows kickstart to
download and run the configuration.

* Download qemu and its dependencies
If you don't have docker, just use centos 7 linux.

```shell
docker run --privileged -it --name qemu -h qemu -v /tmp/qemu:/tmp -v /tmp/.X11-unix:/tmp/.X11-unix:rw --env="DISPLAY" centos:7
yum install -y epel-release
yum install -y which wget lvm2 e2fsprogs qemu-kvm qemu-system-x86
lsmod | grep kvm
```

* Serve the Kickstart File from the Web
yum install -y nginx
cp /tmp/ks.cfg /usr/share/nginx/html/
/usr/sbin/nginx

* Install the qemu runtime dependencies
```shell
yum groupinstall -y "X Window System"
```

* Permit Connections to an X11 Server
```shell
xhost +local:root
```
