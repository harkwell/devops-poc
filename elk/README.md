Build an ELK stack
=================
E = Elasticsearch
L = Logstash
K = Kibana
F = Filebeats

* Provision the Environment

```shell
# dockerd host
sudo bash -o vi

[ $(sysctl -a |grep max_map_count |awk '{print $3}') -lt 262144 ] && {
	sysctl -w vm.max_map_count=262144
}
exit

# base image
export COMPOSE_DOCKER_CLI_BUILD=1
export DOCKER_BUILDKIT=1

docker images |grep -e '^khallware[^A-z]' && {
	docker rmi -f $(docker images |grep -e '^khallware[^A-z]' |awk '{print $1}')
}
docker volume prune -f
docker builder prune -f
docker build -t khallware:latest --target khallware docker/


# elasticsearch

docker volume list -q |grep ^poc-elasticsearch || {
	docker volume create poc-elasticsearch
}
docker images |grep elasticsearch && {
	docker rmi -f $(docker images |grep elasticsearch |awk '{print $1}')
}
export ES_PATH_CONF=/etc/elasticsearch
export ES_JAVA_OPTS=-Xms4g\ -Xmx4g\ -Des.enforce.bootstrap.checks=true
docker build -t elasticsearch:latest --target elasticsearch docker/


# logstash
docker images |grep logstash && {
	docker rmi -f $(docker images |grep logstash |awk '{print $1}')
}
export ES_HOSTS='elasticsearch'
docker build -t logstash:latest --target logstash docker/


# kibana
docker images |grep kibana && {
	docker rmi -f $(docker images |grep kibana |awk '{print $1}')
}
docker build -t kibana:latest --target kibana docker/


# filebeat
docker images |grep filebeat && {
	docker rmi -f $(docker images |grep filebeat |awk '{print $1}')
}
docker build -t filebeat:latest --target filebeat docker/


# orchestrate
yq '.' <docker/docker-compose.yaml |jq -M '.services |keys'
docker-compose -f docker/docker-compose.yaml up
brave-browser http://localhost/


# later, tear down...
docker-compose -f docker/docker-compose.yaml down
docker volume rm poc-elasticsearch
docker rmi -f elasticsearch:latest logstash:latest kibana:latest filebeat:latest
docker rmi -f khallware:latest
```

* Provision kibana

```shell
# menu -> discover (you have data) -> create index "logstash*" -> @timestamp
#      -> create
# menu -> discover
# .............. no workie ...................
# menu -> dashboard -> create -> create panel -> lens -> filters"+" -> save
```

* Produce Some Logs

```shell
docker exec -it filebeat bash
printf '127.0.0.1 - - [%s] "GET /apis/v1/resource HTTP/1.1" 200 9 "-" "agent" 1\n' "$(date)" >>/var/log/test.log
printf 'INFO [%s] my.class.name: some message\n' "$(date)" >>/var/log/test.log
```

* troubleshoot

```shell
# filebeat
docker logs filebeat -f
docker exec -it filebeat bash
telnet logstash 5044

# logstash
brave-browser https://github.com/elastic/logstash/blob/v1.4.2/patterns/grok-patterns#L14
brave-browser https://grokdebug.herokuapp.com/
docker logs logstash -f
docker exec -it logstash netstat -an |grep LISTEN |grep -v ING

# elasticsearch
docker logs elasticsearch -f
docker exec -it elasticsearch netstat -an |grep LISTEN |grep -v ING

# kibana
docker logs kibana -f
docker exec -it kibana bash
```
