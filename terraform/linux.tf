/**
 * Terraform is an infrastructure as code framework.  It can provide
 * middleware for amazon and google so we're "cloud-agnostic" and not
 * dependent on proprietary cloud providers.  In this PoC we're using
 * Amazon Web Services to create one of their VPCs with an EC2 instance
 * that we can ssh into.  If you have a free tier account, this should
 * not cost anything to you.
 *
 */
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_key_path" {}
variable "aws_key_name" {}

provider "aws"
{
	access_key = "${var.aws_access_key}"
	secret_key = "${var.aws_secret_key}"
	region = "us-east-1"
}

resource "aws_vpc" "tfpoc_vpc"
{
	#cidr_block = "${var.vpc_cidr}"
	cidr_block = "192.168.0.0/16"
}

resource "aws_subnet" "tfpoc_dot23"
{
	vpc_id = "${aws_vpc.tfpoc_vpc.id}"
	cidr_block = "192.168.23.0/24"
}

resource "aws_route_table_association" "assocSubnetNetworkACL"
{
	subnet_id = "${aws_subnet.tfpoc_dot23.id}"
	route_table_id = "${aws_route_table.default.id}"
}

resource "aws_internet_gateway" "default"
{
	vpc_id = "${aws_vpc.tfpoc_vpc.id}"
}

resource "aws_route_table" "default"
{
	vpc_id = "${aws_vpc.tfpoc_vpc.id}"

	route
	{
		cidr_block = "0.0.0.0/0"
		gateway_id = "${aws_internet_gateway.default.id}"
	}
}

resource "aws_route_table_association" "default"
{
	subnet_id = "${aws_subnet.tfpoc_dot23.id}"
	route_table_id = "${aws_route_table.default.id}"
}

resource "aws_security_group" "tfpoc_sg"
{
	vpc_id = "${aws_vpc.tfpoc_vpc.id}"

	ingress
	{
		from_port = 22
		to_port = 22
		protocol = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}
}

resource "aws_instance" "tfpoc_ec2"
{
	key_name = "${var.aws_key_name}"
	instance_type = "t2.micro",
	availability_zone = "us-east-1a",
	ami = "ami-cfe4b2b0",
	associate_public_ip_address = true,
	source_dest_check = false
}
