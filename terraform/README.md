Build an AWS VPC with Terraform
=================
First Time
---------------
* Provision a free-tier aws account and a user.

```shell
chromium-browser https://console.aws.amazon.com/ # create a new aws account
# AWS -> IAM -> Users -> Add user -> follow wizard
```

* Provision an EC2 key pair and name it.

```shell
chromium-browser https://console.aws.amazon.com/ # create a new aws account
# AWS -> EC2 -> Key Pairs -> Create Key Pair -> "khallware" -> Create
ls -ld ~/tmp/khallware.pem
```

BUILD
---------------
```shell
# docker (or use centos7)
git clone https://gitlab.com/harkwell/devops-poc && cd devops-poc/terraform
cp ~/tmp/khallware.pem .
docker run -it --privileged --name terraform -h terraform -v $PWD:/tmp centos

# download dependencies
yum install -y epel-release
yum install -y which wget unzip openssh-clients
wget -c 'https://releases.hashicorp.com/terraform/0.11.7/terraform_0.11.7_linux_amd64.zip' -O /tmp/terraform.zip
mkdir -p /usr/local/terraform/bin/
unzip -d /usr/local/terraform/bin /tmp/terraform.zip && rm -f /tmp/terraform.zip
export PATH=$PATH:/usr/local/terraform/bin/
cat <<EOF >/tmp/tfpoc_vars.tfvars
aws_access_key = ""
aws_secret_key = ""
aws_key_path = "/tmp"
aws_key_name = "khallware"
EOF
bash /tmp/build.sh /tmp/tfpoc_vars.tfvars
ssh -i "khallware.pem" ec2-user@SOMEHOSTNAME.compute-1.amazonaws.com

# later
terraform destroy -var-file $TFVARS_FILE
```
