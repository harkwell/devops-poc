#!/bin/bash

D=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
SHCMDS="
	terraform
"
TFVARS_FILE=${1:-/tmp/tfpoc_vars.tfvars}

###################
### SANITYCHECK ###
###################
for cmd in $SHCMDS; do
	which $cmd 2>&1 >/dev/null || { echo "missing: $cmd"; exit 1; }
done
[ -f $D/linux.tf ] || { echo "missing: $D/linux.tf"; exit 1; }
[ -f $TFVARS_FILE ] || { echo "missing: $TFVARS_FILE"; exit 1; }


############
### MAIN ###
############
cd $D && terraform init
terraform plan -var-file $TFVARS_FILE
#terraform plan -var-file $TFVARS_FILE -out /tmp/linux.tfplan
terraform plan -var-file $TFVARS_FILE
terraform apply -var-file $TFVARS_FILE
