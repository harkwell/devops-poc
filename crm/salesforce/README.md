Salesforce
=================
Overview
---------------
From https://www.salesforce.com/:
   Over 150,000 companies use Salesforce CRM to grow their businesses by
   strengthening customer relationships. Customer Relationship Management helps
   companies understand their customers' needs and solve problems by better
   managing customer information and interactions -all on a single platform
   that's always accessible from any desktop or device.

Concepts
---------------
* The UI has two variations: Classic and Lightning (default)
* Org - Fully contains a corporate CRM environment (eg prod, dev, qa).
* Flow - business logic workflows
* sfdxcli - Salesforce DX Command Line Interface (infrastructure as code)
* CRM Objects: lead, account, opportunity, contact, queue
* AppExchange: salesforce plugins that extend org site functionality
* Schema Builder - tool for visualizing the org data model with active ER diagrams


Provision Org
---------------
```shell
# sign-up for a salesforce developer account and create an org
chromium-browser https://developer.salesforce.com/signup
# cloud -> "UserName" -> "My Developer Account"
# GEAR -> Setup -> Find "Dev Hub" -> Enable

# install sfdxcli under centos7 (force:org:open does not function under docker)
#docker run -it -h sfdx --name sfdx -v /tmp/sfdx:/tmp centos:7
yum install -y wget
wget -c 'https://developer.salesforce.com/media/salesforce-cli/sfdx-linux-amd64.tar.xz' -O /tmp/sfdx.txz
mkdir -p /usr/local/sfdx && tar xJf /tmp/sfdx.txz -C /usr/local/sfdx
/usr/local/sfdx/sfdx-cli*/install
export PATH=$PATH:/usr/local/sfdx/bin
sfdx --version
sfdx plugins --core
sfdx plugins
sfdx force --help
sfdx force:doc:commands:list

# persist authentication credentials
mkdir -p /tmp/sfdx && cd /tmp/sfdx
sfdx force:auth:web:login -d -a khall@khallware.com
sfdx force:config:set --global defaultusername=khall@khallware.com
SFURL=$(sfdx force:org:display --verbose |grep ^Sfdx.Auth.Url |awk '{print $4}')
echo $SFURL >/tmp/sfdx/creds.txt

# show the new salesforce org we created
sfdx force:org:list
sfdx force:limits:api:display |grep -i scratch # one can only create so many/day
```


Create App From Site
---------------
```shell
cloud -> Build -> create -> apps -> quick start
```
