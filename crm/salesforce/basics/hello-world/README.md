Salesforce Basics: Hello-world
=================
Overview
---------------
An introduction to the basic salesforce components.


Simple flexipage
---------------
```shell
cd /tmp/sfdx && git clone https://gitlab.com/harkwell/devops-poc.git
cd devops-poc/crm/salesforce/basics/hello-world/

# login via the creds file
sfdx force:auth:sfdxurl:store -f /tmp/sfdx/creds.txt -d -a khall@khallware.com
sfdx force:config:set --global defaultusername=khall@khallware.com

# create the project
sfdx force:project:create --projectname deleteme && cd deleteme

# create the scratch org (NOTE: make sure dev hub is enabled!)
sed -in -e 's#khall Company#khallware#' config/project-scratch-def.json
sfdx force:org:create -f config/project-scratch-def.json --setalias scratch1
SFUSER=$(sfdx force:org:list |grep scratch1 |awk '{print $3}')

# create the flexipage
mkdir -p force-app/main/default/flexipages
cp ../khallware.flexipage-meta.xml force-app/main/default/flexipages/

# commit the project
sfdx force:source:status --targetusername $SFUSER
sfdx force:source:push --targetusername $SFUSER

# open the scratch org
sleep $((4 * 60))  # up to four minutes for salesforce to provision the app
sfdx force:org:open -u $SFUSER

# navigate to "khallware" and click the edit link
platform tools -> user interface -> lightning app builder -> "khallware" -> Edit
```

Mod Page on Site and Pull Changes
---------------
```shell
"add component(s) here" -> Rich Text -> "foo" -> save

# pull and notice the changes
sfdx force:source:pull --targetusername $SFUSER
```
