Salesforce Basics: Flows
=================
Overview
---------------
An introduction to the salesforce flow mechanism.

Concepts
---------------
* Element - panels that visually appear on the canvas (screen, logic, action, integration)
* Connector - the control path taken at runtime (shown as an arrow)
* Resource - containers that represent a given value (eg field value/formula)

Simple flow
---------------
```shell
cd /tmp/sfdx && git clone https://gitlab.com/harkwell/devops-poc.git
cd devops-poc/crm/salesforce/basics/flows/

# login via the creds file
sfdx force:auth:sfdxurl:store -f /tmp/sfdx/creds.txt -d -a khall@khallware.com
sfdx force:config:set --global defaultusername=khall@khallware.com

# create the project
sfdx force:project:create --projectname flow-poc && cd flow-poc

# create the scratch org (NOTE: make sure dev hub is enabled!)
sed -in -e 's#khall Company#khallware#' config/project-scratch-def.json
sfdx force:org:create -f config/project-scratch-def.json --setalias scratch1
SFUSER=$(sfdx force:org:list |grep scratch1 |awk '{print $3}')

# create the flow and flexipage
sed -in -e 's#MYEMAILADDR#'${REPLYTO:-nobody@example.com}'#g' ../khallware.flow-meta.xml
mkdir -p force-app/main/default/flows force-app/main/default/flexipages
cp ../khallware.flow-meta.xml force-app/main/default/flows/
cp ../khallware.flexipage-meta.xml force-app/main/default/flexipages/

# commit the project
sfdx force:source:status --targetusername $SFUSER
sfdx force:source:push --targetusername $SFUSER

# open the scratch org
sleep $((4 * 60))  # up to four minutes for salesforce to provision the app
sfdx force:org:open -u $SFUSER
```

Simple flow (From UI)
---------------
```shell
setup -> process automation -> flows -> new flow

# create the first element
1. drag screen element from palette to canvas
2. add fields

# create the second element
1. drag send email element from palette to canvas
2. add email addresses, body, and subject

# link the two with a connector
1. click and drag the diamond of the first element to the second
2. release when the arrow of the connector reaches the second element

# save the flow and pull
sfdx force:source:pull --targetusername $SFUSER

# activate the flow
setup -> flows -> "khallware" -> activate

# create a home page and add the flow
setup -> user interface -> lightning app builder -> new -> home page -> next
      -> "khallware" -> next -> clone default -> home -> finish
      -> drag flow to upper right area component -> khallware -> save
      -> activate -> next -> activate -> "<- back" -> BLOCKS -> "Home"
      -> FORM: "some text" -> "foo", "some number" -> 2, next
      -> look for email...
sfdx force:source:pull --targetusername $SFUSER
```
