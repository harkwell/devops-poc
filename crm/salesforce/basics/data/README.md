Salesforce Basics: Schema Builder and the Data Model
=================
Overview
---------------
An introduction to the salesforce schema builder mechanism.

Concepts
---------------
* Data Model - RDBMS table relationships, each table represents an entity

Adding a New Object (table)
---------------
```shell
# from developer account
setup -> platform tools -> objects and fields -> schema builder -> clear all
CUBE -> objects -> clear all
CUBE -> elements -> object (drag to canvas) -> FORM:
   label = mystuff
   plural label = mystuff
   object name = mystuff
   record name = name
   data type = text
     -> save
```
