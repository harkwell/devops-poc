Salesforce Basics: SOQL
=================
Overview
---------------
An introduction to the salesforce database query mechanism.

Concepts
---------------
* SOQL - Salesforce Object Query Language (SQL-like)

Query the Databse
---------------
```shell
cd /tmp/sfdx && git clone https://gitlab.com/harkwell/devops-poc.git
cd devops-poc/crm/salesforce/basics/soql/

# login via the creds file
sfdx force:auth:sfdxurl:store -f /tmp/sfdx/creds.txt -d -a khall@khallware.com
sfdx force:config:set --global defaultusername=khall@khallware.com

# run some queries
sfdx force:data:soql:query -q "SELECT Id, Name, Account.Name FROM Contact"

for x in *soql; do
   figlet $x
   sfdx force:data:soql:query -q "$(cat $x)"
   echo ============================================
done
```
