Ory Hydra
=================
URLs
---------------
* https://www.ory.sh/hydra/docs/reference/api
* https://www.ory.sh/hydra
* https://datatracker.ietf.org/doc/html/rfc6749
* https://github.com/ory/hydra
* https://www.ory.sh/run-oauth2-server-open-source-api-security/


Overview
---------------
The mission of Ory is to provide a common access control, authorization and
identity infrastructure that manages IAM and the associated data created in
cloud applications.

PoC
---------------
```shell
export CLNT_ID=myclient
export CLNT_SECRET=deadbeefdeadbeef
docker run --rm -it -e CLNT_ID -e CLNT_SECRET --name hydra -h hydra centos:8 bash

# install version v1.9.0
bash <(curl https://raw.githubusercontent.com/ory/hydra/v1.9.0/install.sh) -b . v1.9.0
mv hydra /usr/local/bin/
hydra version

# provision a database
dnf install -y sqlite-3.26.0 jq
DBDIR=/tmp/hydra-poc/
mkdir -p "$DBDIR"
export DSN="sqlite://$DBDIR/hydra-poc.sqlite?_fk=true"
export SECRETS_SYSTEM=$(export LC_CTYPE=C; cat /dev/urandom |tr -dc 'a-zA-Z0-9' |fold -w 32 |head -n 1)

# start it up
export URLS_SELF_ISSUER=http://khallware.com/
export STRATEGIES_ACCESS_TOKEN=jwt
export LOG_LEAK_SENSITIVE_VALUES=true
export LOG_LEVEL=trace
export OAUTH2_EXPOSE_INTERNAL_ERRORS=true
export OAUTH2_INCLUDE_LEGACY_ERROR_FIELDS=true
hydra migrate sql --yes "$DSN" > /tmp/hydra-migrate.hydra-poc.log
hydra serve all --dangerous-force-http --sqa-opt-out

# provision the test
docker exec -it hydra bash
export HYDRA_URL=http://127.0.0.1:4445
hydra clients list
hydra clients get "$CLNT_ID" # for existing clients
hydra clients delete "$CLNT_ID"
hydra clients create --name "$CLNT_ID" --id "$CLNT_ID" --secret "$CLNT_SECRET" --grant-types client_credentials --response-types token,code --token-endpoint-auth-method client_secret_post
TOKEN=$(hydra token client --client-id "$CLNT_ID" --client-secret "$CLNT_SECRET" --endpoint http://127.0.0.1:4444)
hydra token introspect --client-id "$CLNT_ID" --client-secret "$CLNT_SECRET" "$TOKEN"
echo $TOKEN |cut -d\. -f1 |base64 -d 2>/dev/null |jq '.'
echo $TOKEN |cut -d\. -f2 |base64 -d 2>/dev/null |jq '.'
exit
IPADDR=$(docker inspect hydra |jq '.[]?.NetworkSettings.Networks.bridge.IPAddress' |tr -d \")
AUTH_VAL=$(echo -n ${CLNT_ID}:${CLNT_SECRET} |base64 -w0)==

# test api access
curl -s http://$IPADDR:4444/health/ready |jq -M '.'
curl -s http://$IPADDR:4444/.well-known/jwks.json |jq -M '.'
curl -s http://$IPADDR:4444/.well-known/openid-configuration |jq -M '.'
curl -s -H "Authorization:Basic $AUTH_VAL" http://$IPADDR:4445/clients |jq '.[] |select(.client_name=="myclient")'

# test token creation
BODY="grant_type=client_credentials&client_id=$CLNT_ID&client_secret=$CLNT_SECRET"
JSON=$(curl -X POST -s -H "Authorization:Basic $AUTH_VAL" -H "Content-Type: application/x-www-form-urlencoded" http://$IPADDR:4444/oauth2/token -d "$BODY")
echo "$JSON" |jq -M '.'
TOKEN=$(echo "$JSON" |jq '.access_token' |tr -d \")
echo $TOKEN |cut -d\. -f1 |base64 -d 2>/dev/null |jq '.'
echo $TOKEN |cut -d\. -f2 |base64 -d 2>/dev/null |jq '.'
```
