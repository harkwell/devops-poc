istio (Greek for "sail")
=================
Overview
---------------
The istio platform is a kubernetes (k8s) service mesh.  It empowers one to
-among other things- connect multiple k8s clusters together.  The code here
will stand-up two k8s clusters running on two different docker instances in
two separate networks.  Next, istio will be installed and exercised.


Notes
---------------
* The istio-system namespace is hard-coded.  Do not try to replace.


Provision Two k8s Instances Running Within docker
---------------
```shell
sudo dnf install -y golang
GO111MODULE="on" go get sigs.k8s.io/kind@v0.11.1
export KUBECONFIG=$HOME/tmp/istio-poc-k8s.conf
export PATH=$PATH:$(go env GOPATH)/bin
rm -f "$KUBECONFIG"

kind get clusters |grep k8s1 2>&1 >/dev/null || {
   kind create cluster --name=k8s1
}
kind get clusters |grep k8s2 2>&1 >/dev/null || {
   kind create cluster --name=k8s2
}
kubectl cluster-info --context kind-k8s1
kubectl cluster-info --context kind-k8s2
kubectl config get-contexts
kubectl get nodes --context kind-k8s1
kubectl get nodes --context kind-k8s2
kubectl config use-context kind-k8s1
kubectl get pods --all-namespaces
kubectl create namespace dev --context kind-k8s1
kubectl create namespace dev --context kind-k8s2

# install MetalLB load balancer (in both clusters)
kubectl config use-context kind-k8s1
kubectl config use-context kind-k8s2
printf "$(kubectl get configmap kube-proxy -n kube-system -o json |jq -M '.data."config.conf"')" # strictARP needs to be true
kubectl get configmap kube-proxy -n kube-system -o yaml |sed -e "s#strictARP: false#strictARP: true#" |kubectl apply -f - -n kube-system

kubectl get secret -n dev memberlist || {
        kubectl create secret generic -n dev memberlist --from-literal=secretkey="$(openssl rand -base64 128)"
}
wget -q -c 'https://raw.githubusercontent.com/metallb/metallb/v0.9.6/manifests/metallb.yaml' -O - |sed 's#namespace:.*metallb-system#namespace: dev#g' >/tmp/metallb.yaml
kubectl apply -n dev -f /tmp/metallb.yaml
IPADDR=$(docker inspect k8s1-control-plane |jq '.[]?.NetworkSettings.Networks.kind.IPAddress' |tr -d \")
IPADDR=$(docker inspect k8s2-control-plane |jq '.[]?.NetworkSettings.Networks.kind.IPAddress' |tr -d \")
cat <<EOF |kubectl apply -n dev -f -
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: dev
  name: config
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - $IPADDR-$IPADDR
EOF
kubectl get pods -n dev -l app=metallb
kubectl logs -n dev -l app=metallb --tail=-1 -f

# establish mutual trust: https://istio.io/latest/docs/tasks/security/cert-management/plugin-ca-cert/

mkdir -p /tmp/certs && cd /tmp/certs
#docker run -it --rm -h istio --name istio --link k8s1-control-plane --link k8s2-control-plane --network kind centos:8
docker run -it --rm -h istio --name istio -v /tmp/istio-certs:/tmp/certs centos:8
dnf install -y epel-release
dnf install -y which telnet make openssl
mkdir -p /usr/local && cd /usr/local
curl -L https://istio.io/downloadIstio |sh -
#export PATH=$PATH:$(echo /usr/local/istio-*)/bin
#istioctl install --set profile=istio-poc -y
cd /tmp/certs/
make -f /usr/local/istio*/tools/certs/Makefile.selfsigned.mk root-ca
make -f /usr/local/istio*/tools/certs/Makefile.selfsigned.mk k8s1-cacerts
make -f /usr/local/istio*/tools/certs/Makefile.selfsigned.mk k8s2-cacerts
exit
cd /tmp/istio-certs
sudo chown -R $USER .
kubectl create secret generic cacerts --context kind-k8s1 -n dev \
      --from-file=k8s1/ca-cert.pem \
      --from-file=k8s1/ca-key.pem \
      --from-file=k8s1/root-cert.pem \
      --from-file=k8s1/cert-chain.pem
kubectl create secret generic cacerts --context kind-k8s2 -n dev \
      --from-file=k8s2/ca-cert.pem \
      --from-file=k8s2/ca-key.pem \
      --from-file=k8s2/root-cert.pem \
      --from-file=k8s2/cert-chain.pem

# Install the istio mesh with the control plane on both clusters, making each a primary cluster. Both clusters reside on the khallware network, meaning there is direct connectivity between the pods in both clusters

export CTX_CLUSTER1=kind-k8s1
export CTX_CLUSTER2=kind-k8s2
export CTX_CLUSTER=$CTX_CLUSTER1
export CTX_CLUSTER=$CTX_CLUSTER2

cat <<EOF |istioctl install --context="${CTX_CLUSTER}" -y -f -
apiVersion: install.istio.io/v1alpha1
kind: IstioOperator
spec:
  values:
    global:
      meshID: mesh1
      multiCluster:
        clusterName: $CTX_CLUSTER
      network: khallware
EOF
# don't forget to apply the above to both clusters

# enable endpoint discover
IPADDR=$(docker inspect k8s1-control-plane |jq '.[]?.NetworkSettings.Networks.kind.IPAddress' |tr -d \")
istioctl x create-remote-secret --context="${CTX_CLUSTER1}" --name=k8s1 \
      --server=$IPADDR \
   |sed -e 's#namespace:.*istio-system#namespace: dev#g' \
   |kubectl apply -n dev -f - --context="${CTX_CLUSTER2}"

IPADDR=$(docker inspect k8s2-control-plane |jq '.[]?.NetworkSettings.Networks.kind.IPAddress' |tr -d \")
istioctl x create-remote-secret --context="${CTX_CLUSTER2}" --name=k8s2 \
      --server=$IPADDR \
   |sed -e 's#namespace:.*istio-system#namespace: dev#g' \
   |kubectl apply -n dev -f - --context="${CTX_CLUSTER1}"


# label the namespaces for automatic sidecar injection by istio
kubectl label --context="${CTX_CLUSTER1}" namespace dev \
   istio-injection=enabled
kubectl label --context="${CTX_CLUSTER2}" namespace dev \
   istio-injection=enabled

# deploy a "hello world app" pod to test with
cat <<EOF |kubectl apply -n dev -f -
kind: ConfigMap
apiVersion: v1
metadata:
  name: istio-poc
data:
  index.html: |
    <html>
       <head>
          <title>istio-poc</title>
          <style>body { background-color: #51748e; color: #ffffff; }</style>
       </head>
       <body>This is cluster 1, instance 1.</body>
    </html>
---
apiVersion: apps/v1
kind: Deployment
metadata:
   name: istio-poc
   labels:
      product: khallware
      app: istio-poc
spec:
   replicas: 1
   selector:
      matchLabels:
         product: khallware
         app: istio-poc
   template:
      metadata:
         labels:
            product: khallware
            app: istio-poc
      spec:
         containers:
            - name: istio-poc
              image: "nginx:latest"
              env:
              - name: TZ
                value: CST6CDT
              volumeMounts:
              - name: index-html
                mountPath: /usr/share/nginx/html/index.html
                subPath: index.html
         volumes:
           - name: index-html
             configMap:
               name: istio-poc
               items:
                 - key: index.html
                   path: index.html
---
apiVersion: v1
kind: Service
metadata:
   name: istio-poc
   labels:
      product: khallware
      app: istio-poc
spec:
   selector:
      product: khallware
      app: istio-poc
   type: NodePort
   ports:
      - protocol: TCP
        port: 80
        targetPort: 80
EOF
watch kubectl get svc -n dev
kubectl logs -n dev -l app=istio-poc --tail=-1 -f
POD_NAME=$(kubectl get pods -n dev |grep ^istio-poc |grep Run |awk '{print $1}')
kubectl exec -it -n dev $POD_NAME -- curl localhost

kubectl config use-context kind-k8s2
kubectl get nodes
kubectl get pods --all-namespaces
# deploy a "hello world app" pod to test with
cat <<EOF |kubectl apply -n dev -f -
kind: ConfigMap
apiVersion: v1
metadata:
  name: istio-poc
data:
  index.html: |
    <html>
       <head>
          <title>istio-poc</title>
          <style>body { background-color: #51748e; color: #ffffff; }</style>
       </head>
       <body>This is cluster 2, instance 1.</body>
    </html>
---
apiVersion: apps/v1
kind: Deployment
metadata:
   name: istio-poc
   labels:
      product: khallware
      app: istio-poc
spec:
   replicas: 1
   selector:
      matchLabels:
         product: khallware
         app: istio-poc
   template:
      metadata:
         labels:
            product: khallware
            app: istio-poc
      spec:
         containers:
            - name: istio-poc
              image: "nginx:latest"
              env:
              - name: TZ
                value: CST6CDT
              volumeMounts:
              - name: index-html
                mountPath: /usr/share/nginx/html/index.html
                subPath: index.html
         volumes:
           - name: index-html
             configMap:
               name: istio-poc
               items:
                 - key: index.html
                   path: index.html
---
apiVersion: v1
kind: Service
metadata:
   name: istio-poc
   labels:
      product: khallware
      app: istio-poc
spec:
   selector:
      product: khallware
      app: istio-poc
   type: NodePort
   ports:
      - protocol: TCP
        port: 80
        targetPort: 80
EOF
watch kubectl get svc -n dev
POD_NAME=$(kubectl get pods -n dev |grep ^istio-poc |grep Run |awk '{print $1}')
kubectl exec -it -n dev $POD_NAME -- curl localhost
```

Provision the istio Mesh
---------------
```shell
export K8S_CONTEXT=kind-k8s1
export K8S_CONTEXT=kind-k8s2

# setup the istio gateway, virtual service (do this in both clusters)
cat <<EOF |kubectl apply --context "$K8S_CONTEXT" -n dev -f -
apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: istio-poc
spec:
  selector:
    istio: ingressgateway
  servers:
  - port:
      number: 80
      name: http
      protocol: HTTP
    hosts:
    - "*"
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: istio-poc
spec:
  gateways:
  - istio-poc
  hosts:
  - "*"
  http:
  - match:
    - uri:
        exact: /
    route:
    - destination:
        host: istio-poc
        port:
          number: 80
---
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: istio-poc
spec:
  host: "*"
  trafficPolicy:
    loadBalancer:
      simple: ROUND_ROBIN
  subsets:
  - name: k8s1
    labels:
      product: khallware
      app: istio-poc
  - name: k8s2
    labels:
      product: khallware
      app: istio-poc
EOF
#SVC_NAME=istio-ingressgateway
#kubectl port-forward -n istio-system svc/$SVC_NAME 8080:80
```

Test Out the istio Mesh
---------------
```shell
kubectl get svc istio-ingressgateway -n istio-system -o json |jq '.'
kubectl logs -n istio-system -l app=istiod --tail=-1
kubectl logs -n istio-system -l release=istio --tail=-1 -f

kubectl config use-context kind-k8s1
kubectl config use-context kind-k8s2
IPADDR=$(kubectl get svc -n istio-system istio-ingressgateway -o json |jq '.status.loadBalancer.ingress[]?.ip' |tr -d \")
curl http://$IPADDR/
```

Tear Down Resources
---------------
```shell
kind delete cluster --name k8s1
kind delete cluster --name k8s2
```
