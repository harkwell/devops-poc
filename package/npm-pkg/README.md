nodejs npm package
=================
Overview
---------------
nodejs is a javascript runtime environment and interpretor that leverages
Google's V8 engine outside of a web browser.  An npm (nodejs package manager)
package is a javascript artifact that may be executed by nodejs.  The example
here shows how one may create a simple nodejs npm package.

Proof-of-Concept
---------------
```shell
docker run --rm -it --name delme -h delme centos:8 bash
dnf install -y nodejs
mkdir -p /tmp/src/{poc_module,poc} && cd /tmp/src/poc
npm init -y
echo "console.log('Hello World');" >index.js
node index.js
npm pack
tar zxvf poc-1.0.0.tgz -C ../poc_module/
```
