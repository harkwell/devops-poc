(R)edHat (P)ackage (M)anagement Files
=================
Overview
---------------
RedHat Linux is an American IBM subsidiary software company that provides open
source software products to enterprises.

An RPM file is a package management artifact used for installing specific
versions of RedHat packaged OpenSource software.

Create an RPM
---------------
```shell
# clean up
docker rmi -f rpmfile-poc-build:latest

# build
export COMPOSE_DOCKER_CLI_BUILD=1
export DOCKER_BUILDKIT=1
SEMVER=1.1.0
SHA=$(git log |head -1 |awk '{print $2}' |cut -c-8)
SHA=${SHA:-deadbeef}

# The build images (contains build tools, etc.)
docker build -t rpmfile-poc-build:latest --target rpmfile-poc-build docker/

# Build the RPM
COUNTER=$((COUNTER + 1))
docker run --rm -v $PWD:/tmp/app rpmfile-poc-build:latest /tmp/app/build/build.sh "$SEMVER" "$COUNTER" "$SHA"
rpm -qpi khallware.rpm
rpm -qpl khallware.rpm
```

Install the RPM
---------------
```shell
docker run --rm --name delme -it -v $PWD:/tmp/app rpmfile-poc-build:latest bash
rpm -K /tmp/app/khallware.rpm
dnf install -y /tmp/app/khallware.rpm
cat /opt/khallware/html/index.html
nginx
rpm -V khallware-poc
tail -f /var/log/nginx/*

IPADDR=$(docker inspect delme |jq '.[]?.NetworkSettings.Networks.bridge.IPAddress' |tr -d \")
curl -H "Host: rpmfile-poc.io" http://$IPADDR/
```
