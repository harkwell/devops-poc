#!/bin/bash -x

SCRIPT_DIR="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"

SEMVER=$1
COUNTER=$2
COMMIT_SHA=$3

cd "$SCRIPT_DIR"/../

rm -rf "$SCRIPT_DIR"/../RPMS/
cp SPECS/khallware.spec.template SPECS/khallware-$SEMVER.spec
sed -i -e 's#SEMVER#'$SEMVER'#g' SPECS/khallware-$SEMVER.spec
sed -i -e 's#COUNTER#'$COUNTER'#g' SPECS/khallware-$SEMVER.spec
sed -i -e 's#COMMIT_SHA#'$COMMIT_SHA'#g' SPECS/khallware-$SEMVER.spec

rpmbuild --define "_topdir ${PWD}" -bb SPECS/khallware-$SEMVER.spec
mv "$SCRIPT_DIR"/../RPMS/x86_64/khallware-poc-*rpm khallware.rpm
