#!/bin/sh -x

SCRIPT_DIR="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"

SEMVER=$1
COUNTER=$2
COMMIT_SHA=$3

rm -rf stage/
mkdir -p stage/src/khallware-$SEMVER
mkdir -p ~/packages/pocuser
cd stage/

cp ../khallware.template APKBUILD
sed -i -e 's#SEMVER#'$SEMVER'#g' APKBUILD
sed -i -e 's#COUNTER#'$COUNTER'#g' APKBUILD
sed -i -e 's#COMMIT_SHA#'$COMMIT_SHA'#g' APKBUILD

abuild checksum
abuild -v -r
mv ~/packages/pocuser/x86_64/khallware-$SEMVER-r$COUNTER.apk $SCRIPT_DIR/../
