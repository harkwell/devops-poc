(A)lpine (P)ac(k)age Management File (APK)
=================
Overview
---------------
Alpine Linux is a security-oriented, lightweight Linux distribution based on
musl libc and busybox.

An APK file is a package management artifact used for installing specific
versions of software.

Create an APK
---------------
```shell
# clean up
docker rmi -f apkbuild-poc-build:latest

# build
export COMPOSE_DOCKER_CLI_BUILD=1
export DOCKER_BUILDKIT=1
SEMVER=1.1.0
SHA=$(git log |head -1 |awk '{print $2}' |cut -c-8)
SHA=${SHA:-deadbeef}

# The build images (contains build tools, etc.)
docker build -t apkbuild-poc-build:latest --target apkbuild-poc-build docker/

# Build the APK
COUNTER=$((COUNTER + 1))
docker run --rm -v $PWD:/tmp/app apkbuild-poc-build:latest su - pocuser -c "/tmp/app/build/build.sh \"$SEMVER\" \"$COUNTER\" \"$SHA\""
```

Install the APK
---------------
```shell
docker run --rm --name delme -it -v $PWD:/tmp/app apkbuild-poc-build:latest sh
cd /tmp/app
apk add khallware*.apk
cat /usr/share/nginx/html/index.html
mkdir /run/nginx
chown nginx /run/nginx
vi /etc/nginx/conf.d/default.conf # replace "return 404" with "root /usr/share/nginx/html;"
nginx
tail -f /var/log/nginx/*

IPADDR=$(docker inspect delme |jq '.[]?.NetworkSettings.Networks.bridge.IPAddress' |tr -d \")
brave-browser http://$IPADDR/
```
