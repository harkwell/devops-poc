# .bashrc

if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

alias ls='ls -aCF'
alias more=less

export LC_COLLATE=C
export TZ=CST6CDT
export LC_CTYPE=C
export PS1='\[\033[33m\]\h \w $\[\033[0m\] '
