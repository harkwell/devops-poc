haproxy
=================
Overview
---------------
haproxy includes an OSI layer 7 proxy redirect.

Build
---------------
```shell
docker images |grep haproxy && {
   docker rmi -f haproxy-poc-nginx haproxy
}
export POCTYPE=fanout
export POCTYPE=pathreg
docker-compose -f docker/docker-compose.yaml up
IPADDR=$(docker inspect haproxy |jq '.[]?.NetworkSettings.Networks.khallware.IPAddress' |tr -d \")
```

Simple Fan-Out
---------------
```shell
curl -H "Host: site1" http://$IPADDR/site1/index-site1.html
```

Regular Expression Matching and Replacement in the URI Path
---------------
```shell
curl -H "Host: site1" http://$IPADDR/apis/v2/khall-index-site1.html
curl -H "Host: site1" http://$IPADDR/site1/apis/v2/khall-index-site1.html

curl -H "Host: site2" http://$IPADDR/apis/v2/foo-index-site2.html
curl -H "Host: site2" http://$IPADDR/site2/apis/v2/foo-index-site2.html
```

Troubleshooting
---------------
```shell
#docker exec -it haproxy bash
docker exec -it haproxy sh
set -o vi
alias more=less
alias ls='ls -aCF'
haproxy -version
haproxy -vv
```
