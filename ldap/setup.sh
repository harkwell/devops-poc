#!/bin/bash

CMDLIST="
	ldapadd
"

which which 2>&1 >/dev/null || { echo "/bin/which is not installed"; exit 1; }

for cmd in $CMDLIST; do
	which $cmd >/dev/null || { echo "$cmd is not installed"; exit 1; }
done

PASSWD=${1:-ui87PlszeIp230Pexdporl}

echo create the ldap domain controller
cat <<'EOF' >/tmp/khallware.ldif
dn: dc=khallware,dc=com
objectClass: dcObject
objectClass: organization
dc: khallware
o : khallware
EOF
ldapadd -f /tmp/khallware.ldif -D cn=Manager,dc=khallware,dc=com -w $PASSWD

echo create the ou - organizational unit
cat <<'EOF' >/tmp/users.ldif
dn: ou=Users,dc=khallware,dc=com
objectClass: organizationalUnit
ou: Users
EOF
ldapadd -f /tmp/users.ldif -D cn=Manager,dc=khallware,dc=com -w $PASSWD

echo load the schema dependencies
ldapadd -f /etc/openldap/schema/cosine.ldif -D cn=config -w $PASSWD
ldapadd -f /etc/openldap/schema/nis.ldif -D cn=config -w $PASSWD
ldapadd -f /etc/openldap/schema/inetorgperson.ldif -D cn=config -w $PASSWD
