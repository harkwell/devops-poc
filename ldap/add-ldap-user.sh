#!/bin/bash

CMDLIST="
	ldapadd
"

which which 2>/dev/null || { echo "/bin/which is not installed"; exit 1; }

for cmd in $CMDLIST; do
	which $cmd >/dev/null || { echo "$cmd is not installed"; exit 1; }
done

USERNAME=${1}
USER=${2}
USERPASS=${3}
PASSWD=${4:-ui87PlszeIp230Pexdporl}

[ "$USERNAME" ] || { echo "missing username"; exit 1; }
[ "$USER" ] || { echo "missing real name 'first last'"; exit 1; }
[ "$USERPASS" ] || { echo "missing users password"; exit 1; }

echo adding user $USERNAME
cat <<-EOF >/tmp/$USERNAME.ldif
dn: cn=$USER,ou=Users,dc=khallware,dc=com
cn: $USER
sn: $(echo $USER |sed 's# *$##' |sed 's#^.* ##')
objectClass: inetOrgPerson
userPassword: $USERPASS
uid : $USERNAME
EOF
ldapadd -f /tmp/$USERNAME.ldif -D cn=Manager,dc=khallware,dc=com -w $PASSWD
