#!/bin/bash

CMDLIST="
	slappasswd sed echo
"

which which 2>&1 >/dev/null || { echo "/bin/which is not installed"; exit 1; }

for cmd in $CMDLIST; do
	which $cmd >/dev/null || { echo "$cmd is not installed"; exit 1; }
done

CFG=/etc/openldap/slapd.d/cn\=config/olcDatabase\={0}config.ldif
PASSWD=${1:-ui87PlszeIp230Pexdporl}
ENCPASSWD=$(slappasswd -s $PASSWD)
echo 'olcRootDN: cn=config' >>$CFG
echo "olcRootPW: $ENCPASSWD" >>$CFG

CFG=/etc/openldap/slapd.d/cn\=config/olcDatabase\={2}hdb.ldif
sed -in -e 's#^olcSuffix.*$#olcSuffix: dc=khallware,dc=com#' \
   -e 's#^olcRootDN.*$#olcRootDN: cn=Manager,dc=khallware,dc=com#' $CFG
echo "olcRootPW: $ENCPASSWD" >>$CFG

CFG=/etc/openldap/ldap.conf
echo 'TLS_REQCERT allow' >>$CFG

CFG=/etc/openldap/slapd.d/cn\=config/olcDatabase\={1}monitor.ldif
sed -in -e 's#"cn=Manager.*"#"cn=Manager,dc=khallware,dc=com"#g' $CFG
