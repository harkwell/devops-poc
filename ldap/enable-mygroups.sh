#!/bin/bash

CMDLIST="
	ldapadd
"

which which 2>/dev/null || { echo "/bin/which is not installed"; exit 1; }

for cmd in $CMDLIST; do
	which $cmd >/dev/null || { echo "$cmd is not installed"; exit 1; }
done

PASSWD=${1:-ui87PlszeIp230Pexdporl}

echo adding mygroups ou at the root of the dit
cat <<-EOF >/tmp/mygroups.ldif
dn: ou=mygroups,dc=khallware,dc=com
objectclass:organizationalunit
ou: mygroups
description: mygroups ou at the root of the dit
EOF
ldapadd -f /tmp/mygroups.ldif -D cn=Manager,dc=khallware,dc=com -w $PASSWD
