#!/bin/bash

CMDLIST="
	ldapadd
"

which which 2>/dev/null || { echo "/bin/which is not installed"; exit 1; }

for cmd in $CMDLIST; do
	which $cmd >/dev/null || { echo "$cmd is not installed"; exit 1; }
done

GROUPNAME=${1}
PEOPLE=${2}
PASSWD=${3:-ui87PlszeIp230Pexdporl}

[ "$GROUPNAME" ] || { echo "missing groupname"; exit 1; }
[ "$PEOPLE" ] || { echo "missing real name 'first last'"; exit 1; }

echo adding group $GROUPNAME
cat <<EOF >/tmp/$GROUPNAME.ldif
dn: cn=$GROUPNAME,ou=mygroups,dc=khallware,dc=com
objectclass: groupofnames
cn: $GROUPNAME
description: mygroups group named $GROUPNAME
# add the group members all of which are 
# assumed to exist under people
EOF

for user in $(echo $PEOPLE |sed -e 's#:# #g'); do
	[ -z $user ] && continue
	echo member: cn=$(echo $user |tr _ ' '),ou=Users,dc=khallware,dc=com >>/tmp/$GROUPNAME.ldif
done
grep ^member /tmp/$GROUPNAME.ldif >/dev/null \
	&& ldapadd -f /tmp/$GROUPNAME.ldif -D cn=Manager,dc=khallware,dc=com -w $PASSWD
