Stand up an LDAP server via Docker
=================
PROVISION
---------------
```shell
docker run -it --rm --name ldap -h ldap -v $PWD:/tmp centos:7
yum install -y openlap openldap-servers openldap-clients which
cd /tmp/ldap
export PASSWD=mypassword
./init.sh $PASSWD
/usr/sbin/slapd -d 256 -F /etc/openldap/slapd.d

docker exec -it ldap bash
export PASSWD=mypassword
cd /tmp/ldap
./setup.sh $PASSWD
./add-ldap-user.sh khall "Kevin D. Hall" "somepasswd" $PASSWD

# for groups
./enable-mygroups.sh $PASSWD
./add-people-to-groups.sh developers Kevin_Hall:Wiley_Coyote $PASSWD
```

TEST
---------------
```shell
IPADDR=$(docker inspect ldap |grep IPAddress |tail -1 |cut -d\"  -f4)
export PASSWD=mypassword
ldapsearch -x -H "ldap://$IPADDR/" -LLL -b dc=khallware,dc=com -D cn=Manager,dc=khallware,dc=com -w $PASSWD

# for groups
ldapsearch -x -H ldap://$IPADDR -b dc=khallware,dc=com -D cn=Manager,dc=khallware,dc=com -w $PASSWD "(objectClass=groupOfNames)"
```
