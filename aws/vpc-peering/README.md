aws vpc peering
=================
Overview
---------------
Amazon Web Services (aws) Virtual Private Cloud (vpc) peering is where two
or more vpcs pass internet traffic between them.  This can be set up via the
aws Command Line Interface (cli).

Provision Foundational Cloud Resources
---------------
```shell
# provision the aws cli (using docker)

docker images |grep awscli || {
	export COMPOSE_DOCKER_CLI_BUILD=1
	export DOCKER_BUILDKIT=1
	docker build -t awscli:latest --target awscli ../docker/
}
docker run -it --rm --name awscli -h awscli -v $HOME/.aws:/root/.aws awscli:latest bash

# validate that the aws cli is provisioned correctly by listing the account
aws organizations list-accounts

# choose VPCs, regions, EC2 sizes, etc.
VPC1=delme1
VPC1_REGION=us-east-2
EC2HOST1=$VPC1
NETADDR1=10.23.23.0

VPC2=delme2
VPC2_REGION=us-west-2
EC2HOST2=$VPC2
NETADDR2=10.33.33.0

#..... copy ssh key for the EC2 instances .....#
KEYNAME=delme
PUBFILE=/tmp/khall-eks-node.pub
docker cp ~/.ssh/aws/khall-eks-node.pub awscli:/tmp/khall-eks-node.pub

# locate an Amazon Machine Identifier (ami)
VPC1_AMI_ID=ami-000e7ce4dd68e7a11 # centos 8
REGION=$VPC1_REGION
AMI_ID=$VPC1_AMI_ID

VPC2_AMI_ID=ami-095b4698501e46899 # ubuntu
REGION=$VPC2_REGION
AMI_ID=$VPC2_AMI_ID

aws ec2 describe-images --region "$REGION" --filters "Name=architecture,Values=x86_64" "Name=state,Values=available" |jq -M '.Images[0:10]'
aws ec2 describe-images --region "$REGION" --image-ids "$AMI_ID" |jq -M '.Images[0]'


# provision the two VPCs each having their own distinct region
EC2HOST=$VPC1
/usr/local/bin/setup-ec2-host.sh "$VPC1" "$VPC1_REGION" "$NETADDR1" \
	"$EC2HOST" "$KEYNAME" "$VPC1_AMI_ID" t2.micro "$PUBFILE"

EC2HOST=$VPC2
/usr/local/bin/setup-ec2-host.sh "$VPC2" "$VPC2_REGION" "$NETADDR2" \
	"$EC2HOST" "$KEYNAME" "$VPC2_AMI_ID" t2.micro "$PUBFILE"

# provision the ssh config (repeat for each EC2 instance)
VPC=$VPC1
REGION=$VPC1_REGION

VPC=$VPC2
REGION=$VPC2_REGION

EC2HOST=$VPC
JSON=$(aws ec2 describe-instances --region "$REGION" |jq '.Reservations[]?.Instances[] |select((.Tags[]? |select(.Key=="Name")|.Value) |match("'$EC2HOST'"))')
EC2HOST_ID=$(echo $JSON |jq '.InstanceId' |tr -d \")
IPADDR=$(echo $JSON |jq '.PublicIpAddress' |grep -v null |tr -d \")

for x in centos ubuntu admin fedora bitnami ec2-user root; do
	EC2USER=$x
done
#cat <<EOF >>~/.ssh/config
cat <<EOF
Host $EC2HOST
Hostname $IPADDR
User $EC2USER
IdentityFile ~/.ssh/aws/khall-eks-node
EOF
```

Test the Sad Path
---------------
```shell
# ssh into both EC2 instances
screen
ssh delme1
sudo bash -o vi
dnf install -y epel-release
dnf install -y figlet nmap-ncat net-tools
ifconfig eth0 # take note of the IPADDR
netstat -rn
figlet hello world |ncat -l -p 7
# ctrl-a c
ssh delme2
sudo bash -o vi
IPADDR=10.23.23.183 # the delme1 eth0 ip address
ip route
telnet "$IPADDR" 7 # notice it times out.... make peering connection, try again
# ctrl-a n
```

Create the Peering Connection
---------------
```shell
aws ec2 create-vpc-peering-connection \
	--vpc-id "$VPC1_ID" --region "$VPC1_REGION" \
	--peer-vpc-id "$VPC2_ID" --peer-region "$VPC2_REGION"
PEERING_ID=$(aws ec2 describe-vpc-peering-connections --region "$VPC1_REGION" |jq '.VpcPeeringConnections[]?.VpcPeeringConnectionId' |tr -d \")

aws ec2 accept-vpc-peering-connection --region "$VPC2_REGION" \
	--vpc-peering-connection-id "$PEERING_ID"

# update the route tables to pass traffic between the EC2 instances
VPC=$VPC1
REGION=$VPC1_REGION
ROUTETBL="$VPC"-route-table
ROUTETBL_ID=$(aws ec2 describe-route-tables --region "$REGION" --filters "Name=tag:Name,Values=$ROUTETBL" |jq '.RouteTables[0]?.RouteTableId' |tr -d \")
aws ec2 create-route --region "$REGION" --route-table-id "$ROUTETBL_ID" \
	--destination-cidr-block "$NETADDR2"/24 \
	--vpc-peering-connection-id "$PEERING_ID"

VPC=$VPC2
REGION=$VPC2_REGION
ROUTETBL="$VPC"-route-table
ROUTETBL_ID=$(aws ec2 describe-route-tables --region "$REGION" --filters "Name=tag:Name,Values=$ROUTETBL" |jq '.RouteTables[0]?.RouteTableId' |tr -d \")
aws ec2 create-route --region "$REGION" --route-table-id "$ROUTETBL_ID" \
	 --destination-cidr-block "$NETADDR1"/24 \
	--vpc-peering-connection-id "$PEERING_ID"


# open ports between EC2 instances
VPC=$VPC1
REGION=$VPC1_REGION
SECTY_GRP="$VPC"-securitygroup
SECTY_GRP_ID=$(aws ec2 describe-security-groups --region "$REGION" |jq '.SecurityGroups[] |select((.Tags[]? |select(.Key=="Name")|.Value) |match("'$SECTY_GRP'")) |.GroupId' |tr -d \")
aws ec2 authorize-security-group-ingress --region "$REGION" \
   --group-id "$SECTY_GRP_ID" --protocol tcp --port 7 --cidr 0.0.0.0/0

# Now, test the "Sad Path" from delme2 again... it should be "Happy"!
telnet "$IPADDR" 7
```

Tear Down Resources
---------------
```shell
# delete the peering connection
PEERING_ID=$(aws ec2 describe-vpc-peering-connections --region "$VPC1_REGION" |jq '.VpcPeeringConnections[]?.VpcPeeringConnectionId' |tr -d \")

aws ec2 delete-vpc-peering-connection --region "$VPC2_REGION" \
	--vpc-peering-connection-id "$PEERING_ID"

EC2HOST=$VPC1
/usr/local/bin/remove-ec2-host.sh "$VPC1" "$VPC1_REGION" \
	"$EC2HOST" "$KEYNAME"

EC2HOST=$VPC2
/usr/local/bin/remove-ec2-host.sh "$VPC2" "$VPC2_REGION" \
	"$EC2HOST" "$KEYNAME"
```
