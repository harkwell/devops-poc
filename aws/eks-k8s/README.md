EKS (Elastic Kubernetes Service)
=================
Overview
---------------
Amazon Web Services (AWS) Elastic Kubernetes (k8s) Service (EKS) is a platform
as a service offering.  The example here provisions k8s on an Elastic Cloud
Computing (EC2) Virtual Machine (VM).

Help was proved from: https://www.golinuxcloud.com/setup-kubernetes-cluster-on-aws-ec2/


Provision the Two EC2 Instances
---------------
```shell
# provision the aws cli (using docker)

docker images |grep awscli || {
	export COMPOSE_DOCKER_CLI_BUILD=1
	export DOCKER_BUILDKIT=1
	docker build -t awscli:latest --target awscli ../aws/docker/
}
docker run -it --rm --name awscli -h awscli -v $HOME/.aws:/root/.aws awscli:latest bash

# validate that the aws cli is provisioned correctly by listing the account
aws organizations list-accounts

# choose VPCs, regions, EC2 sizes, etc.
KEYNAME=delme

VPC1=delme1
VPC1_REGION=us-east-2
EC2HOST1=$VPC1
NETADDR1=10.23.23.0

VPC2=delme2
VPC2_REGION=us-west-2
EC2HOST2=$VPC2
NETADDR2=10.33.33.0

#..... copy ssh key for the EC2 instances .....#
PUBFILE=/tmp/khall-eks-node.pub
docker cp ~/.ssh/aws/khall-eks-node.pub awscli:/tmp/khall-eks-node.pub

# locate an Amazon Machine Identifier (ami)
VPC1_AMI_ID=ami-000e7ce4dd68e7a11 # centos 8.2
REGION=$VPC1_REGION
AMI_ID=$VPC1_AMI_ID

VPC2_AMI_ID=ami-00055f73272833276 # centos 8
REGION=$VPC2_REGION
AMI_ID=$VPC2_AMI_ID

aws ec2 describe-images --region "$REGION" --filters "Name=architecture,Values=x86_64" "Name=state,Values=available" |jq -M '.Images[0:10]'
aws ec2 describe-images --region "$REGION" --image-ids "$AMI_ID" |jq -M '.Images[0]'


# provision the two VPCs each having their own distinct region
EC2HOST=$VPC1
/usr/local/bin/setup-ec2-host.sh "$VPC1" "$VPC1_REGION" "$NETADDR1" \
	"$EC2HOST" "$KEYNAME" "$VPC1_AMI_ID" t2.large "$PUBFILE"

EC2HOST=$VPC2
/usr/local/bin/setup-ec2-host.sh "$VPC2" "$VPC2_REGION" "$NETADDR2" \
	"$EC2HOST" "$KEYNAME" "$VPC2_AMI_ID" t2.large "$PUBFILE"

# enable DNS support and hostnames (in both VPCs)
VPC=$VPC1
REGION=$VPC1_REGION

VPC=$VPC2
REGION=$VPC2_REGION

VPC_ID=$(aws ec2 describe-vpcs --region "$REGION" |jq -M '.Vpcs[] |select((.Tags[]? |select(.Key=="Name")|.Value) |match("'$VPC'")) |.VpcId' |tr -d \")
aws ec2 modify-vpc-attribute --enable-dns-support --region "$REGION" \
	--vpc-id "$VPC_ID"
aws ec2 modify-vpc-attribute --enable-dns-hostnames --region "$REGION" \
	--vpc-id $VPC_ID


# add tags to the VPCs
aws ec2 create-tags --resources $VPC_ID --region "$REGION" \
	--tags Key=kubernetes.io/cluster/golinuxcloud,Value=shared


# provision private route tables and private subnets?

# provision the ssh config (repeat for each EC2 instance)
VPC=$VPC1
REGION=$VPC1_REGION

VPC=$VPC2
REGION=$VPC2_REGION

EC2HOST=$VPC
JSON=$(aws ec2 describe-instances --region "$REGION" |jq '.Reservations[]?.Instances[] |select((.Tags[]? |select(.Key=="Name")|.Value) |match("'$EC2HOST'"))')
EC2HOST_ID=$(echo $JSON |jq '.InstanceId' |tr -d \")
IPADDR=$(echo $JSON |jq '.PublicIpAddress' |grep -v null |tr -d \")

for x in centos ubuntu admin fedora bitnami ec2-user root; do
	EC2USER=$x
done
#cat <<EOF >>~/.ssh/config
cat <<EOF
Host $EC2HOST
Hostname $IPADDR
User $EC2USER
IdentityFile ~/.ssh/aws/khall-eks-node
EOF
```

Install docker and k8s
---------------
```shell
# ssh into both EC2 instances
screen
ssh delme1
sudo bash -o vi
vi /etc/selinux/config # disable
reboot # ssh and become root again
# install dependencies and tools not included in the ami
dnf install -y epel-release
dnf install -y figlet nmap-ncat net-tools tc nfs-utils net-tools lsof telnet
dnf install -y chrony jq wget
# provision the OS
swapoff -a
systemctl enable --now chronyd
dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
# install docker community edition
dnf install -y docker-ce
mkdir -p /etc/docker
cat <<'EOF' >/etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "insecure-registries" : ["docker-repo"]
}
EOF
systemctl enable docker
systemctl start docker
docker images
# install kubernetes
cat <<EOF >/etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF
dnf install -y iproute-tc kubelet kubeadm kubectl --disableexcludes=kubernetes
systemctl status kubelet
systemctl enable kubelet
systemctl start kubelet
systemctl restart chronyd
kubeadm config images pull
docker images
kubeadm init --token-ttl 0 --pod-network-cidr 10.244.0.0/16 # --ignore-preflight-errors
# take note of the join token, it will look similar to:
cat <<EOF # this is for worker nodes (we won't set up any, the master will work)
kubeadm join 10.23.23.192:6443 --token pc7ynt.tbj1t7vlny0xqvq1 \
        --discovery-token-ca-cert-hash sha256:148a65d6774ccc1d84c27dc7f96ddff8bd8ff702a9949bcb5c940737127ed138
EOF
export KUBECONFIG=/etc/kubernetes/admin.conf
kubectl get nodes # should just be the master node at the moment
cp $KUBECONFIG /tmp/
chmod 777 /tmp/admin.conf
scp delme1:/tmp/admin.conf /tmp/delme1-k8s-kube.conf
# install the container network interface (CNI)
# .............. no workie ................
kubectl apply -f https://raw.githubusercontent.com/aws/amazon-vpc-cni-k8s/master/config/v1.9/aws-k8s-cni.yaml
# .........................................
kubectl apply -f https://raw.githubusercontent.com/haouc/amazon-vpc-cni-k8s/master/config/master/aws-k8s-cni.yaml
kubectl taint nodes --all node-role.kubernetes.io/master-
kubectl taint nodes --all node.kubernetes.io/not-ready-
rm /var/lib/kubelet/kubeadm-flags.env
systemctl restart kubelet
kubectl get pods --all-namespaces
# deploy a "hello world app" pod to test with
cat <<EOF |kubectl apply -n dev -f -
apiVersion: v1
kind: Namespace
metadata:
   name: dev
   labels:
      name: dev
---
kind: ConfigMap
apiVersion: v1
metadata:
  name: istio-poc
data:
  index.html: |
    <html>
       <head>
          <title>istio-poc</title>
          <style>body { background-color: #51748e; color: #ffffff; }</style>
       </head>
       <body>This is cluster 1, instance 1.</body>
    </html>
---
apiVersion: apps/v1
kind: Deployment
metadata:
   name: istio-poc
   labels:
      product: khallware
      app: istio-poc
spec:
   replicas: 1
   selector:
      matchLabels:
         product: khallware
         app: istio-poc
   template:
      metadata:
         labels:
            product: khallware
            app: istio-poc
      spec:
         containers:
            - name: istio-poc
              image: "nginx:latest"
              env:
              - name: TZ
                value: CST6CDT
              volumeMounts:
              - name: index-html
                mountPath: /usr/share/nginx/html/index.html
                subPath: index.html
         volumes:
           - name: index-html
             configMap:
               name: istio-poc
               items:
                 - key: index.html
                   path: index.html
---
apiVersion: v1
kind: Service
metadata:
   name: istio-poc
   labels:
      product: khallware
      app: istio-poc
spec:
   selector:
      product: khallware
      app: istio-poc
   type: LoadBalancer
   ports:
      - protocol: TCP
        port: 80
        targetPort: 80
EOF
watch kubectl get svc -n dev
POD_NAME=$(kubectl get pods -n dev |grep ^istio-poc |grep Run |awk '{print $1}')
kubectl exec -it -n dev $POD_NAME -- curl localhost
curl $(kubectl get endpoints -n dev istio-poc -o json |jq '.subsets[]?.addresses[]?.ip' |tr -d \")
# install MetalLB load balancer
printf "$(kubectl get configmap kube-proxy -n kube-system -o json |jq -M '.data."config.conf"')" # strictARP needs to be true
kubectl get configmap kube-proxy -n kube-system -o yaml |sed -e "s#strictARP: false#strictARP: true#" |kubectl apply -f - -n kube-system
for f in /proc/sys/net/ipv4/conf/*/proxy_arp; do echo 1 >$f; done

kubectl get secret -n dev memberlist || {
        kubectl create secret generic -n dev memberlist --from-literal=secretkey="$(openssl rand -base64 128)"
}
wget -q -c 'https://raw.githubusercontent.com/metallb/metallb/v0.9.6/manifests/metallb.yaml' -O - |sed 's#namespace:.*metallb-system#namespace: dev#g' >/tmp/metallb.yaml
kubectl apply -n dev -f /tmp/metallb.yaml
IPADDR=$(hostname -s |sed 's#^ip\-##' |tr '-' '.')
cat <<EOF |kubectl apply -n dev -f -
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: dev
  name: config
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - $IPADDR-$IPADDR
EOF
curl $IPADDR
kubectl get pods -n dev -l app=metallb
kubectl logs -n dev -l app=metallb --tail=-1 -f
kubectl logs -n dev -l app=istio-poc --tail=-1 -f
# test that the cluster can be reached from the Internet
ssh -L 8080:10.23.23.192:80 -L 6443:10.23.23.192:6443 delme1
brave-browser http://localhost:8080/
# ............. no workie ..............
cp /etc/kubernetes/kubelet.conf /tmp/delme1-kube.conf
chmod 777 /tmp/delme1-kube.conf
cd /var/lib/kubelet/pki/
tar cvhf /tmp/delme1-pki.tar .
chmod 777 /tmp/delme1-pki.tar
scp delme1:/tmp/delme1-kube.conf /tmp/
scp delme1:/tmp/delme1-pki.tar /tmp/
mkdir /tmp/delme1-pki && cd /tmp/delme1-pki
tar xvf /tmp/delme1-pki.tar .

echo 'display the x509 certifcate for the "kubernetes-admin" user'
yq '.' </tmp/delme-kube.conf |jq '.users[]?.user."client-certificate-data"' |tr -d \" |base64 -d |openssl x509 -text -in -

echo 'display the private key for the "kubernetes-admin" user'
yq '.' </tmp/delme-kube.conf |jq '.users[]?.user."client-key-data"' |tr -d \" |base64 -d |openssl rsa -text -in -

echo 'display the cluster x509 certifcate'
yq '.' </tmp/delme-kube.conf |jq '.clusters[]?.cluster."certificate-authority-data"' |tr -d \" |base64 -d |openssl x509 -text -in -
export KUBECONFIG=/tmp/delme1-kube.conf
sed -i -e 's#/var/lib/kubelet/pki/#/tmp/delme1-pki/#g' "$KUBECONFIG"
sed -i -e 's#10.23.23.192#127.0.0.1#g' "$KUBECONFIG"
kubectl get nodes
# ............. no workie ..............
K8S_SVC_CLUSTER_IP=$(kubectl get svc -n default kubernetes -o json |jq '.spec.clusterIP' |tr -d \") # 10.96.0.1
K8S_SVC_CLUSTER_IP=${K8S_SVC_CLUSTER_IP:-10.96.0.1}
cat <<EOF >/tmp/localhost.csr.conf
[ req ]
default_bits = 2048
prompt = no
default_md = sha256
req_extensions = req_ext
distinguished_name = dn

[ dn ]
C = USA
ST = TN
L = Franklin
O = khallware
OU = istio
CN = 127.0.0.1

[ req_ext ]
subjectAltName = @alt_names

[ alt_names ]
DNS.1 = kubernetes
DNS.2 = kubernetes.default
DNS.3 = kubernetes.default.svc
DNS.4 = kubernetes.default.svc.cluster
DNS.5 = kubernetes.default.svc.cluster.local
IP.1 = 127.0.0.1
IP.2 = $K8S_SVC_CLUSTER_IP

[ v3_ext ]
authorityKeyIdentifier=keyid,issuer:always
basicConstraints=CA:FALSE
keyUsage=keyEncipherment,dataEncipherment
extendedKeyUsage=serverAuth,clientAuth
subjectAltName=@alt_names
EOF
openssl req -new -key /etc/kubernetes/pki/apiserver.key -out /tmp/localhost.csr -config /tmp/localhost.csr.conf
kubectl certificate approve /tmp/localhost.csr
# ............. no workie ..............
cat $KUBECONFIG |sed 's#10.23.23.192#127.0.0.1#g' >/tmp/delme1-kube.conf
scp delme1:/tmp/delme1-kube.conf /tmp/

echo 'display the x509 certifcate for the "kubernetes-admin" user'
yq '.' </tmp/delme-kube.conf |jq '.users[]?.user."client-certificate-data"' |tr -d \" |base64 -d |openssl x509 -text -in -

echo 'display the private key for the "kubernetes-admin" user'
yq '.' </tmp/delme-kube.conf |jq '.users[]?.user."client-key-data"' |tr -d \" |base64 -d |openssl rsa -text -in -

echo 'display the cluster x509 certifcate'
yq '.' </tmp/delme-kube.conf |jq '.clusters[]?.cluster."certificate-authority-data"' |tr -d \" |base64 -d |openssl x509 -text -in -
# ............. no workie ..............
cp /etc/kubernetes/pki/ca.key /tmp && chmod 777 /tmp/ca.key # from delme1
mkdir /tmp/delme1-config && cd /tmp/delme1-config
scp delme1:/tmp/ca.key /tmp/delme-ca.key
kubectl config --kubeconfig=delme1-k8s.conf set-cluster development --server=https://127.0.0.1:6443 --certificate-authority=/tmp/delme-ca.key
export KUBECONFIG=/tmp/delme1-config/delme1-k8s.conf
kubectl config set-credentials developer --client-certificate=fake-cert-file --client-key=fake-key-seefile

# ............. no workie ..............
cat $KUBECONFIG |sed 's#10.23.23.192#127.0.0.1#g' >/tmp/delme-kube.conf
scp delme1:/tmp/delme-kube.conf /tmp/
scp delme1:/etc/kubernetes/pki/apiserver.crt /tmp/
scp delme1:/etc/kubernetes/pki/apiserver.key /tmp/
export KUBECONFIG=/tmp/delme-kube.conf
kubectl get nodes
kubectl get pods -n dev
# ctrl-a c
# perform the same for delme2
```

Tear Down Resources
---------------
```shell
EC2HOST=$VPC1
/usr/local/bin/remove-ec2-host.sh "$VPC1" "$VPC1_REGION" "$EC2HOST" "$KEYNAME"

EC2HOST=$VPC2
/usr/local/bin/remove-ec2-host.sh "$VPC2" "$VPC2_REGION" "$EC2HOST" "$KEYNAME"
```
