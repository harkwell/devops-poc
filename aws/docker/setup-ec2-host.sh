#/bin/bash

SCRIPT_DIR="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"
SCRIPT_CMDS="figlet aws sed jq mktemp echo grep tr rm"

VPC=${1:-delme}
VPC_REGION=${2:-us-east-2}
NETADDR=$3
EC2HOST=${4:-$VPC}
KEYNAME=${5:-$VPC}
AMI_ID=${6:-deadbeef}
VM_SIZE=${7:-t2.micro}
PUBFILE=${8:-/tmp/khall-eks-node.pub}

export AWS_SHARED_CREDENTIALS_FILE=${AWS_SHARED_CREDENTIALS_FILE:-/root/.aws/credentials}

###################
### SANITYCHECK ###
###################
which which >/dev/null 2>&1 || { echo the which command is not found; exit 1; }

for command in $SCRIPT_CMDS; do
	which $command >/dev/null 2>&1 || {
		echo the \"$command\" command was not found
		exit 1
	}
done
[ -f "$PUBFILE" ] || {
	echo The SSH public key file \"$PUBFILE\" must exist.
	exit 1
}
LIST=$(aws ec2 describe-regions |jq '.Regions[]?.RegionName' |tr -d \" |sort -r)

for REGION in $LIST; do
	[ "$VPC_ID" ] || {
		VPC_ID=$(aws ec2 describe-vpcs --region "$REGION" |jq -M '.Vpcs[] |select((.Tags[]? |select(.Key=="Name")|.Value) |match("'$VPC'")) |.VpcId' |tr -d \")
		FOUND=$REGION
	}
done
[ "$FOUND" -a "$FOUND" != "$VPC_REGION" ] && {
	echo Warning: a VPC named \"$VPC\" was also found in region \"$FOUND\"
}
REGION=$VPC_REGION


############
### MAIN ###
############
[ "$VPC_ID" ] || {
	echo Warning: the \"$VPC\" VPC was not found, it will be created...
	echo The new VPC will be created in region \"$REGION\"
	TAGSPEC="ResourceType=vpc,Tags=[{Key=Name,Value=$VPC}]"
	aws ec2 create-vpc --region "$VPC_REGION" --cidr-block "$NETADDR/16" \
		--tag-specifications "$TAGSPEC"
}
#..... create the required internet gateway (repeat for each VPC) .....#

IPGW_ID=$(aws ec2 describe-internet-gateways --region "$REGION" --filters "Name=tag:Name,Values=$VPC" |jq '.InternetGateways[]?.InternetGatewayId' |tr -d \")
[ "$IPGW_ID" == "null" ] && IPGW_ID=

[ "$IPGW_ID" ] && {
	echo WARN: the ip gateway \"$IPGW_ID\" already exists.
}
[ "$IPGW_ID" ] || {
	echo Creating an Internet Gateway in VPC $VPC $VPC_ID
	TAGSPEC="ResourceType=internet-gateway,Tags=[{Key=Name,Value=$VPC}]"
	aws ec2 create-internet-gateway --region "$REGION" \
		--tag-specifications "$TAGSPEC"
}
IPGW_ID=$(aws ec2 describe-internet-gateways --region "$REGION" --filters "Name=tag:Name,Values=$VPC" |jq '.InternetGateways[].InternetGatewayId' |tr -d \")
[ "$IPGW_ID" == "null" ] && IPGW_ID=

aws ec2 describe-internet-gateways --region "$REGION" \
			--filters "Name=tag:Name,Values=$VPC" \
		|jq '.InternetGateways[]?.Attachments[]?.VpcId' \
		|grep "$VPC_ID" >/dev/null || {
	echo Attaching the ip gateway to the vpc \"$VPC\"
	aws ec2 attach-internet-gateway --region "$REGION" --vpc-id "$VPC_ID" \
		--internet-gateway-id "$IPGW_ID"
}

#..... create the required subnet .....#
SUBNET="$VPC"-subnet
REGION=$VPC_REGION
CIDR_BLOCK=${NETADDR}/24

SUBNET_ID=$(aws ec2 describe-subnets --region "$REGION" |jq '.Subnets[] |select((.Tags[]? |select(.Key=="Name")|.Value) |match("'$SUBNET'")) |.SubnetId' |tr -d \")
[ "$SUBNET_ID" == "null" ] && SUBNET_ID=
FLAG=

[ "$SUBNET_ID" ] && {
	echo WARN: subnet \"$SUBNET\" has already been created in \"$REGION\"
}
[ "$SUBNET_ID" ] || {
	echo Creating the \"$VPC\" vpc subnet \"$SUBNET\"
	TAGSPEC="ResourceType=subnet,Tags=[{Key=Name,Value=$SUBNET}]"
	aws ec2 create-subnet --region "$REGION" --vpc-id "$VPC_ID" \
		--cidr-block "$CIDR_BLOCK" \
		--availability-zone "$REGION"a --tag-specifications "$TAGSPEC"
	SUBNET_ID=$(aws ec2 describe-subnets --region "$REGION" |jq '.Subnets[] |select((.Tags[]? |select(.Key=="Name")|.Value) |match("'$SUBNET'")) |.SubnetId' |tr -d \")
	[ "$SUBNET_ID" == "null" ] && SUBNET_ID=
	FLAG=1
}


#..... create the required route table .....#
ROUTETBL="$VPC"-route-table
REGION=$VPC_REGION
FLAG=
ROUTETBL_ID=$(aws ec2 describe-route-tables --region "$REGION" --filters "Name=tag:Name,Values=$ROUTETBL" |jq '.RouteTables[0]?.RouteTableId' |tr -d \")
[ "$ROUTETBL_ID" == "null" ] && ROUTETBL_ID=

[ "$ROUTETBL_ID" ] && {
	echo WARN: The route table \"$ROUTETBL\" already exists in \"$REGION\".
}
[ "$ROUTETBL_ID" ] || {
	echo Creating a route table that includes internet gateway in vpc $VPC
	TAGSPEC="ResourceType=route-table,Tags=[{Key=Name,Value=$ROUTETBL}]"
	aws ec2 create-route-table --region "$REGION" --vpc-id "$VPC_ID" \
		--tag-specifications "$TAGSPEC"
	FLAG=1
}
ROUTETBL_ID=$(aws ec2 describe-route-tables --region "$REGION" --filters "Name=tag:Name,Values=$ROUTETBL" |jq '.RouteTables[0]?.RouteTableId' |tr -d \")
[ "$ROUTETBL_ID" == "null" ] && ROUTETBL_ID=

[ "$ROUTETBL_ID" -a "$FLAG" ] && {
	echo Creating route to the Internet
	IPGW_ID=$(aws ec2 describe-internet-gateways --region "$REGION" --filters "Name=tag:Name,Values=$VPC" |jq '.InternetGateways[].InternetGatewayId' |tr -d \")
	aws ec2 create-route --region "$REGION" --route-table-id "$ROUTETBL_ID" \
		--destination-cidr-block 0.0.0.0/0 --gateway-id "$IPGW_ID"
}


#..... associate the subnet to a route table .....#
ROUTETBL="$VPC"-route-table
REGION=$VPC_REGION
SUBNET_ID=$(aws ec2 describe-subnets --region "$REGION" |jq '.Subnets[] |select((.Tags[]? |select(.Key=="Name")|.Value) |match("'$SUBNET'")) |.SubnetId' |tr -d \")

[ "$SUBNET_ID" ] && {
	ROUTETBL_ID=$(aws ec2 describe-route-tables --region "$REGION" --filters "Name=tag:Name,Values=$ROUTETBL" |jq '.RouteTables[0]?.RouteTableId' |tr -d \")
	aws ec2 associate-route-table --region "$REGION" \
		--route-table-id "$ROUTETBL_ID" --subnet-id "$SUBNET_ID"
}


#..... create a security group .....#
SECTY_GRP="$VPC"-securitygroup
REGION=$VPC_REGION
SECTY_GRP_ID=$(aws ec2 describe-security-groups --region "$REGION" |jq '.SecurityGroups[] |select((.Tags[]? |select(.Key=="Name")|.Value) |match("'$SECTY_GRP'")) |.GroupId' |tr -d \")
[ "$SECTY_GRP_ID" == "null" ] && SECTY_GRP_ID=
FLAG=

[ "$SECTY_GRP_ID" ] && {
	echo WARN: security group \"$SECTY_GRP\" already exists in \"$REGION\".
}
[ "$SECTY_GRP_ID" ] || {
	echo Creating the security group
	TAGSPEC="ResourceType=security-group,Tags=[{Key=Name,Value=$SECTY_GRP}]"
	aws ec2 create-security-group --region "$REGION" --vpc-id "$VPC_ID" \
		--description "security group $SECTY_GRP" \
		--group-name "$SECTY_GRP" --tag-specifications "$TAGSPEC"
	SECTY_GRP_ID=$(aws ec2 describe-security-groups --region "$REGION" |jq '.SecurityGroups[] |select((.Tags[]? |select(.Key=="Name")|.Value) |match("'$SECTY_GRP'")) |.GroupId' |tr -d \")
	[ "$SECTY_GRP_ID" == "null" ] && SECTY_GRP_ID=
	FLAG=1
}

[ "$SECTY_GRP_ID" -a "$FLAG" ] && {
	echo Authorizing ingress and egress for security group
	aws ec2 authorize-security-group-ingress --region "$REGION" \
		--group-id "$SECTY_GRP_ID" --protocol tcp --port 22 \
		--cidr 0.0.0.0/0
}
#..... upload ssh key for the EC2 instance .....#

aws ec2 describe-key-pairs --region "$REGION" |jq '.KeyPairs[]?.KeyName' \
			|tr -d \" |grep ^"$KEYNAME"$ >/dev/null || {
	echo Creating the SSH key pair \"$KEYNAME\" in \"$REGION\".
	aws ec2 import-key-pair --region "$REGION" --key-name "$KEYNAME" \
		--public-key-material file://"$PUBFILE"
}


#..... launch a single EC2 instance .....#
SUBNET="$VPC"-subnet
REGION=$VPC_REGION
JSON=$(aws ec2 describe-instances --region "$REGION" |jq '.Reservations[]?.Instances[] |select((.Tags[]? |select(.Key=="Name")|.Value) |match("'$EC2HOST'"))')
EC2HOST_ID=$(echo $JSON |jq '.InstanceId' |tr -d \")
[ "$EC2HOST_ID" == "null" ] && EC2HOST_ID=

[ "$EC2HOST_ID" ] && {
	echo WARN: The EC2 host \"$EC2HOST_ID\" already exists in \"$REGION\".
}
[ "$EC2HOST_ID" ] || {
	TAGSPEC="ResourceType=instance,Tags=[{Key=Name,Value=$EC2HOST}]"
	SUBNET_ID=$(aws ec2 describe-subnets --region "$REGION" |jq '.Subnets[] |select((.Tags[]? |select(.Key=="Name")|.Value) |match("'$SUBNET'")) |.SubnetId' |tr -d \")
	SECTY_GRP="$VPC"-securitygroup
	SECTY_GRP_ID=$(aws ec2 describe-security-groups --region "$REGION" |jq '.SecurityGroups[] |select((.Tags[]? |select(.Key=="Name")|.Value) |match("'$SECTY_GRP'")) |.GroupId' |tr -d \")
	echo Launching EC2 \"$EC2HOST\" in VPC \"$VPC\" of region \"$REGION\".

	aws ec2 run-instances --region "$REGION" --image-id "$AMI_ID" \
		--instance-type "$VM_SIZE" --key-name "$KEYNAME" \
		--associate-public-ip-address --subnet-id "$SUBNET_ID" \
		--security-group-ids "$SECTY_GRP_ID" --count 1 \
		--tag-specifications "$TAGSPEC" \
		--subnet-id "$SUBNET_ID"

	JSON=$(aws ec2 describe-instances --region "$REGION" |jq '.Reservations[]?.Instances[] |select((.Tags[]? |select(.Key=="Name")|.Value) |match("'$EC2HOST'"))')
	EC2HOST_ID=$(echo $JSON |jq '.InstanceId' |tr -d \")
	aws ec2 wait instance-running --region "$REGION" \
		--instance-ids "$EC2HOST_ID"

	# turn off source-dest checking (grab EC2HOST_ID first)
	aws ec2 modify-instance-attribute --instance-id "$EC2HOST_ID" \
		--region "$REGION" --no-source-dest-check
}
