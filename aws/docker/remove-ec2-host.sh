#/bin/bash

SCRIPT_DIR="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"
SCRIPT_CMDS="figlet aws sed jq mktemp echo grep tr rm"

VPC=${1:-delme}
VPC_REGION=${2:-us-east-2}
EC2HOST=${3:-$VPC}
KEYNAME=${4:-$VPC}

REGION=$VPC_REGION

export AWS_SHARED_CREDENTIALS_FILE=${AWS_SHARED_CREDENTIALS_FILE:-/root/.aws/credentials}

###################
### SANITYCHECK ###
###################
which which >/dev/null 2>&1 || { echo the which command is not found; exit 1; }

for command in $SCRIPT_CMDS; do
	which $command >/dev/null 2>&1 || {
		echo the \"$command\" command was not found
		exit 1
	}
done
[ "$VPC_ID" ] || {
	VPC_ID=$(aws ec2 describe-vpcs --region "$REGION" |jq -M '.Vpcs[] |select((.Tags[]? |select(.Key=="Name")|.Value) |match("'$VPC'")) |.VpcId' |tr -d \")

	[ "$VPC_ID" ] || {
		echo ERROR: The VPC \"$VPC\" was not found in region \"$REGION\"
		exit 1
	}
}
JSON=$(aws ec2 describe-instances --region "$REGION" |jq '.Reservations[]?.Instances[] |select((.Tags[]? |select(.Key=="Name")|.Value) |match("'$EC2HOST'"))')
EC2HOST_ID=$(echo $JSON |jq '.InstanceId' |tr -d \")
EC2STATE=$(echo $JSON |jq '.State.Name' |tr -d \")
[ "$EC2HOST_ID" == "null" ] && EC2HOST_ID=

[ "$EC2HOST_ID" ] || {
	echo WARN: The EC2 VM \"$EC2HOST\" was not found in region \"$REGION\"
}


############
### MAIN ###
############
# delete the EC2 instance (repeat for each VPC)

[ "$EC2HOST_ID" -a "$EC2STATE" != "terminated" ] && {
	echo INFO: terminating EC2 host id \"$EC2HOST_ID\"
	aws ec2 terminate-instances --region "$REGION" \
		--instance-ids "$EC2HOST_ID"
	aws ec2 wait instance-terminated --region "$REGION" \
		--instance-ids "$EC2HOST_ID"
}

# delete the security group
SECTY_GRP="$VPC"-securitygroup
SECTY_GRP_ID=$(aws ec2 describe-security-groups --region "$REGION" |jq '.SecurityGroups[] |select((.Tags[]? |select(.Key=="Name")|.Value) |match("'$SECTY_GRP'")) |.GroupId' |tr -d \")

[ "$SECTY_GRP_ID" ] || {
	echo WARN: The security group \"$SECTY_GRP\" was not found.
}
[ "$SECTY_GRP_ID" ] && {
	echo INFO: revoking security group ingress/egress
	aws ec2 revoke-security-group-ingress --region "$REGION" \
		--group-id "$SECTY_GRP_ID" --protocol tcp --port 22 \
		--cidr 0.0.0.0/0
	aws ec2 revoke-security-group-egress --region "$REGION" \
		--group-id "$SECTY_GRP_ID" \
		--ip-permissions IpProtocol=-1,FromPort=0,ToPort=0,IpRanges='[{CidrIp=0.0.0.0/0}]'
	echo INFO: deleting the security group \"$SECTY_GRP\" from \"$REGION\"
	aws ec2 delete-security-group --region "$REGION" \
		--group-id "$SECTY_GRP_ID"
}

# delete the subnet
REGION=$VPC_REGION
SUBNET="$VPC"-subnet
SUBNET_ID=$(aws ec2 describe-subnets --region "$REGION" |jq '.Subnets[] |select((.Tags[]? |select(.Key=="Name")|.Value) |match("'$SUBNET'")) |.SubnetId' |tr -d \")

[ "$SUBNET_ID" ] || {
	echo WARN: The subnet \"$SUBNET\" was not found in region \"$REGION\".
}
[ "$SUBNET_ID" ] && {
	echo INFO: deleting the subnet \"$SUBNET\" from \"$REGION\"
	aws ec2 delete-subnet --region "$REGION" --subnet-id "$SUBNET_ID"
}

# delete the route table
ROUTETBL="$VPC"-route-table
ROUTETBL_ID=$(aws ec2 describe-route-tables --region "$REGION" --filters "Name=tag:Name,Values=$ROUTETBL" |jq '.RouteTables[0]?.RouteTableId' |tr -d \")
[ "$ROUTETBL_ID" == "null" ] && ROUTETBL_ID=

[ "$ROUTETBL_ID" ] || {
	echo WARN: The route table \"$ROUTETBL\" was not found.
}
[ "$ROUTETBL_ID" ] && {
	echo INFO: deleting the route table \"$ROUTETBL\" from \"$REGION\"
	aws ec2 delete-route-table --region "$REGION" \
		--route-table-id "$ROUTETBL_ID"
}

# delete the ip gateway
IPGW_ID=$(aws ec2 describe-internet-gateways --region "$REGION" --filters "Name=tag:Name,Values=$VPC" |jq '.InternetGateways[].InternetGatewayId' |tr -d \")

[ "$IPGW_ID" ] || {
	echo WARN: The ip gateway \"$VPC\" was not found in region \"$REGION\".
}
[ "$IPGW_ID" == "null" ] && IPGW_ID=

[ "$IPGW_ID" ] && {
	echo INFO: detaching and deleting the ipgw \"$IPGW_ID\" from \"$REGION\"
	aws ec2 detach-internet-gateway --region "$REGION" --vpc-id "$VPC_ID" \
		--internet-gateway-id "$IPGW_ID"
	aws ec2 delete-internet-gateway --region "$REGION" \
		--internet-gateway-id "$IPGW_ID"
}

# delete the ssh keys
echo INFO: deleting the SSH key \"$KEYNAME\" from \"$REGION\"
aws ec2 delete-key-pair --region "$REGION" --key-name "$KEYNAME"

# delete the VPC
echo INFO: deleting the VPC \"$VPC\" from \"$REGION\"
aws ec2 delete-vpc --region "$REGION" --vpc-id "$VPC_ID"
