Developer Operations
=================
Overview
---------------
These examples show various aspects of devops and linux.

Examples include:

* [Kubernetes](kubernetes) - Kubernetes basics, recipes cluster
* [Linux Minmal](linux-min) - Build A Virtual Machine Image with linux
* [LDAP](ldap) - Stand up an LDAP server via Docker
* [Packer](packer) - Build A Virtual Machine Image with packer
* [Kickstart](kickstart-centos) - Build A Centos7 Virtual Machine Image
* [Terraform](terraform) - Build an AWS VPC with Terraform
