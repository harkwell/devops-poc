mongodb
=================
Overview
---------------
mongodb is a schemaless document database.  Documents adhere to the json
format.

Build
---------------
```shell
openssl rand -base64 756 >docker/khallware-keyfile.b64
chmod 400 docker/khallware-keyfile.b64
docker-compose -f docker/docker-compose.yaml up
```

Access and Initiate Replication (from mongo-1)
---------------
```shell
docker exec -it mongo-1 bash
mongo -u root -p admin --host mongo-1 --authenticationDatabase "admin"
rs.initiate({
  _id :  "khallware-replicaset",
  members: [
    { _id:  0, host:  "mongo-1:27017" },
    { _id:  1, host:  "mongo-2:27017" },
    { _id:  2, host:  "mongo-3:27017" }
  ]
})
rs.conf()
use khallware
db.khallware.insert({id:0,name:"foo",desc:"some description"})
db.khallware.find({},{_id:0}).pretty()

docker exec -it mongo-2 bash
mongo -u root -p admin --host mongo-2 --authenticationDatabase "admin"
use khallware
db.khallware.find({},{_id:0}).pretty()

docker exec -it mongo-3 bash
mongo
use khallware
db.khallware.find({},{_id:0}).pretty()
```
