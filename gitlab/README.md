Build a docker instance with gitlab
=================

* Installation

```shell
brave-browser https://about.gitlab.com/installation/#centos-7 \
                 https://hub.docker.com/r/gitlab/gitlab-ce/

#docker run -it -h gitlab --name gitlab -v /tmp/gitlab:/tmp centos:7
#yum install -y curl policycoreutils-python iproute
#curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh |bash
#export EXTERNAL_URL=http://$(ip addr |grep inet |grep -v -e inet6 -e 127.0.0.1 |cut -d/ -f1 |awk '{print $2}')
#yum install -y gitlab-ee
#
# OR
docker pull gitlab/gitlab-ce
docker run -it -h gitlab --name gitlab \
    --publish 443:443 --publish 8080:80 --publish 2222:22 \
    --volume /tmp/gitlab/config:/etc/gitlab \
    --volume /tmp/gitlab/logs:/var/log/gitlab \
    --volume /tmp/gitlab/data:/var/opt/gitlab \
    gitlab/gitlab-ce

sleep $((15 * 60 * 60)) # fifteen minutes, maybe longer
#IPADDR=$(docker inspect gitlab |grep IPAddress |head -1 |cut -d\" -f4)
#brave-browser https://$IPADDR/
brave-browser http://localhost:8080/  # set password (>= 8 chars)
export PASSWD=blahblah
curl -s "http://localhost:8080/api/v4/projects" |jq . |more
# FOX -> Projects -> Your Projects
```

* Create a User

```shell
curl -s "http://root:$PASSWD@localhost:8080/api/v4/users" |jq . |more
# FOX -> Wrench -> New User -> "khallware"
```

* Create a Group

```shell
# FOX -> Wrench -> New Group -> "khallware"
```

* Create a Project and Import

```shell
cd /tmp/ && git clone https://gitlab.com/harkwell/khallware.git
# FOX -> Wrench -> New Project -> "khallware", public, uncheck "init", create
# OR
curl -H "Content-Type: application/json" -X POST "http://root:$PASSWD@localhost:8080/api/v4/projects/user/1" -d '{ \"name\" : \"khallware\" }'

cd khallware && git remote add http://localhost:8080/
```

* Integration with LDAP

```shell
export COMPOSE_DOCKER_CLI_BUILD=1
export DOCKER_BUILDKIT=1

docker-compose -f docker-compose.yaml up
docker exec -it ldap bash
setup.sh $PASSWD
add-ldap-user.sh khall "Kevin D. Hall" "somepasswd" $PASSWD
enable-mygroups.sh $PASSWD
add-people-to-groups.sh developers Kevin_Hall:Wiley_Coyote $PASSWD

# connect to gitlab and sign in as root/foofoofoo
docker exec -it gitlab bash
gitlab-rails console
user = User.find_by(id: 1)
pp user.attributes
user.password = 'foofoofoo'
user.password_confirmation = 'foofoofoo'
user.skip_confirmation!
user.save!
brave-browser http://localhost:8080/

# provision gitlab to pull users from LDAP
# .... no workie ...........
docker exec -it gitlab bash
cd /etc/gitlab/
cp gitlab.rb $(date +%Y%m%d)-gitlab.rb
cat <<'EOF' >> gitlab.rb
gitlab_rails['ldap_enabled'] = true
gitlab_rails['ldap_host'] = 'ldap'
gitlab_rails['ldap_port'] = 389
gitlab_rails['ldap_uid'] = 'uid'
gitlab_rails['ldap_method'] = 'plain' # 'ssl' or 'plain'
gitlab_rails['ldap_bind_dn'] = 'CN=Manager,DC=khallware,DC=com'
gitlab_rails['ldap_password'] = 'mypassword'
gitlab_rails['ldap_allow_username_or_email_login'] = true
gitlab_rails['ldap_base'] = 'DC=khallware,DC=com'
EOF

vi gitlab.rb # ldap_sync_worker_cron "5,10,15,20,25,30,35,40,45,50,55 * * * *"
gitlab-ctl reconfigure
grep ldap /var/opt/gitlab/gitlab-rails/etc/gitlab.yml # make sure is turned on
gitlab-ctl restart
watch gitlab-ctl status
gitlab-ctl tail

gitlab-rake gitlab:ldap:check
apt-get update
apt-get install -y ldap-utils
IPADDR=ldap
PASSWD=mypassword
ldapsearch -x -H "ldap://$IPADDR/" -LLL -b dc=khallware,dc=com -D cn=Manager,dc=khallware,dc=com -w $PASSWD

# logout and sign in as khall/somepassword
brave-browser http://localhost:8080/
```
