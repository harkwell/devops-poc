#!/bin/bash

PRODFS=/tmp/prodfs
DEVFS=/tmp/devfs
UFSROOT=/tmp/ufsroot

[ $(id -u) -eq 0 ] || { echo must be root to mount filesystems; exit 1; }

mkdir -p $PRODFS || { echo failed to create $PRODFS; exit 1; }
mkdir -p $DEVFS || { echo failed to create $DEVFS; exit 1; }
mkdir -p $UFSROOT || { echo failed to create $UFSROOT; exit 1; }

printf "this\nis\nfile1\n" >$PRODFS/file1
printf "this\nis\nfile1 again\n" >$DEVFS/file1
printf "this\nis\nfile2\n" >$DEVFS/file2

mount -t aufs -o br=$DEVFS:$PRODFS none $UFSROOT
ls $UFSROOT
cat $UFSROOT/file1
