Deploy khallware recipes
=================

Provision Kubernetes (Optional)
---------------
```shell
# kubeadm
node1 # [ $(id -u) -eq 0 ] && kubeadm init --pod-network-cidr 192.168.23.0/24 # notice the join command printed out
node2 # kubeadm join <IPADDR>:6443 --token <TOKEN> --discovery-token-ca-cert-hash sha256:<GUID>
kubectl apply --filename https://git.io/weave-kube-1.6

# OR
# minicube
export MINIKUBE_HOME=/tmp/minikube
minikube start

## ping (cluster api)
#export KUBECONFIG=/tmp/admin.conf
kubectl cluster-info
kubectl get nodes
```

Build the appserver Docker Image (Once)
---------------
```shell
# minicube
export MINIKUBE_HOME=/tmp/minikube
eval $(minikube docker-env)
docker images |grep ware-recipes.*v1.1 >/dev/null || echo 'BUILD IMAGE FIRST'
TOPDIR=$(mktemp -d)
cp Dockerfile recipe.yaml $TOPDIR/
cd $TOPDIR/ && docker build -t khallware-recipes:v1.1 .
cd - && rm -rf $TOPDIR

# kubeadm
docker build -t khallware-recipes:v1.1 .
docker save -o /tmp/khallware.tgz khallware-recipes:v1.1
scp /tmp/khallware.tgz node1:/tmp
ssh node1
sudo bash -o vi
docker load -i /tmp/khallware.tgz
```

DEPLOY
---------------
```shell
# minikube
eval $(minikube docker-env)
docker images |grep ware-recipes.*v1.1 >/dev/null || echo 'BUILD IMAGE FIRST'

## create a persistent volume for mariadb (unless using minikube)
cat <<EOF >/tmp/pv-local.yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-local
  labels:
     type: local
spec:
  capacity:
    storage: 11Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: /mnt/khallware
EOF
kubectl apply -f /tmp/pv-local.yaml
rm /tmp/pv-local.yaml
kubectl get pv


## deploy the mariadb database service
cd /tmp
git clone https://github.com/alvistack/docker-mariadb.git && cd docker-mariadb
git checkout 10.3.8-0alvistack1
sed -i -e 's#^piVersion#apiVersion#' kubernetes/mariadb-sts.yml
kubectl apply -f kubernetes/.
cd - && rm -rf /tmp/docker-mariadb/


## grant permissions for appserver to connect to miriadb
kubectl exec -it mariadb-0 -- /bin/bash
printenv |grep ^MYSQL_ROOT_PASSWORD |cut -d= -f2
MYPASS=$(!!)
mysql -u root -p$MYPASS
CREATE DATABASE recipes;
CREATE USER 'api'@'%' IDENTIFIED BY 'khallware';
GRANT ALL PRIVILEGES ON recipes.* TO 'api'@'%' WITH GRANT OPTION;
USE mysql;
SET PASSWORD FOR 'api'@'%' = PASSWORD('khallware');
exit


## deploy the pod
kubectl run khallware --image=khallware-recipes:v1.1 --port=8080

## get status of the running docker containers
kubectl get deployments

## what is our pod name?
kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}'
export POD_NAME=$(!!)

kubectl expose deployment khallware --port=30080 --external-ip=192.168.23.XX --type=NodePort --name khallware-service
kubectl get services
kubectl describe services khallware-service
#minikube service khallware-service --url
ssh node2
#find node
#IPADDR=$(kubectl describe pod khallware |grep ^IP: |cut -d: -f2 |sed 's# ##g')
IPADDR=$(kubectl get pods --output=wide |grep ^khallware |awk '{print $6}')

## connect and talk to the recipe application
#MYURL=$(minikube service khallware-service --url)
MYPORT=$(kubectl get services khallware-service |grep ^khall |awk '{print $5}' |cut -d: -f2 |cut -d/ -f1)
MYURL=http://node1:$MYPORT
MYURL=http://node2:$MYPORT
curl -X POST -H "Content-Type: application/json" -d '{ "name":"recipe1" }' $MYURL/recipes/
curl $MYURL/recipes/ # beware of "double slashes in the URL"
curl $MYURL/recipes/recipe1
curl -X DELETE $MYURL/recipes/recipe1
```

TROUBLESHOOTING
---------------
```shell
### is the cluster functioning
# is the VM running
ps -ef |grep minikube # notice virtualbox/VBoxHeadless
minikube ssh
ps -ef |grep docker
docker ps
docker inspect $CONTAINER_ID

# check the REST api
kubectl version

# what cluster nodes are available
kubectl get nodes

### what pods are running and where
kubectl get pods -o wide

## connect to minikube (and info related to)
minikube ip
minikube ssh
minikube dashboard
minikube service list
minikube docker-env
kubectl get events

### pods
# for Running pods, connect and inspect
kubectl exec -it mariadb-0 -- /bin/bash
mysql -umariadb -pUGFzc3cwcmQh  mysql

# for pods with issues, dig deeper
kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}'
export POD_NAME=$(!!)
kubectl describe pod $POD_NAME

### connect to a node and inspect logs on the container
eval $(minikube docker-env)
CONTAINER_ID=$(docker ps |grep khallware |awk '{print $1}')
docker inspect $CONTAINER_ID
docker exec -it $CONTAINER_ID bash

# it may be necessary to stop the deployment and re-run with bash
kubectl delete service khallware-service
kubectl delete deployment khallware
#kubectl run khallware --image=khallware-recipes:v1.1 -it
kubectl run khallware --image=khallware-recipes:v1.1 -it --command bash
kubectl get pods # look at the status (should say "Running")
export POD_NAME=$(kubectl get pods |grep ^khall |awk '{print $1}')
kubectl attach $POD_NAME -c khallware -i -t
```

DESTROY
---------------
```shell
kubectl delete service khallware-service
kubectl delete service mariadb
kubectl delete service mariadb-cluster
kubectl delete deployment khallware
kubectl delete statefulset mariadb
kubectl delete pvc mariadb-pvc-var-lib-mysql-mariadb-0
kubectl delete pv pv-nfs
kubectl delete pv pv-local
#kubectl delete pod mariadb-0
kubectl get statefulset
kubectl get replicaset
kubectl get deployments
kubectl get services
kubectl get pods
kubectl get pvc
kubectl get pv
node2 # kubeadm reset
minikube stop

### clean up any kubernetes ISO images that were downloaded
minikube delete
UUID=$(VBoxManage list vms |grep minikube |tail -1 |sed 's#^.*{\(.*\)}$#\1#')
#VBoxManage storagectl $UUID --name SATA --remove
VBoxManage unregistervm $UUID --delete
rm -rf $MINIKUBE_HOME ~/.minikube/
```
