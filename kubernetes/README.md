Kubernetes Examples
=================
Overview
---------------
These examples show various aspects of Kubernetes clusters.

Examples include:

* [Basics](basics) - Host an echo server within a minikube cluster.
* [Recipes](recipes) - Host the khallware recipes java application.


Quick Start
---------------
```shell
cat /etc/redhat-release # CentOS Stream release 8
sudo dnf install -y golang
GO111MODULE="on" go get sigs.k8s.io/kind@v0.11.1
export KUBECONFIG=$HOME/tmp/istio-poc-k8s.conf
export PATH=$PATH:$(go env GOPATH)/bin
rm -f "$KUBECONFIG"

kind get clusters |grep k8s 2>&1 >/dev/null || {
   kind create cluster --name=k8s
}
kubectl get nodes
kubectl get pods --all-namespaces

# tear it down
kind delete cluster --name k8s
```
