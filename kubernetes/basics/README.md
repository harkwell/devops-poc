Deploy an echo server within kubernetes
=================

INSTALL
---------------
```shell
### install (Centos7)
#yum install -y epel-release
yum install -y kubernetes-client
curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.28.2/minikube-linux-amd64 && chmod +x minikube && mv minikube /usr/local/bin/

### install (debian)
sudo apt-get update && sudo apt-get install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo touch /etc/apt/sources.list.d/kubernetes.list 
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl

curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.28.2/minikube-linux-amd64 && chmod +x minikube && mv minikube /usr/local/bin/
```

DEPLOY
---------------
```shell
### provision an arbitrary kubernetes cluster (that runs docker images)
export MINIKUBE_HOME=/tmp/minikube
minikube start
VBoxManage list vms
kubectl config get-contexts
ps -ef |grep minikube # notice virtualbox/VBoxHeadless
ls -ld ~/.kube/config $MINIKUBE_HOME/.minikube/config/

## ping (version)
kubectl version

## ping (cluster api)
kubectl cluster-info

## show connected nodes
kubectl get nodes

## build a docker image
TOPDIR=$(mktemp -d)
cat <<EOF >$TOPDIR/Dockerfile
FROM centos
RUN yum install -y nc
EXPOSE 4
CMD nc -l -p 4 -c 'xargs -n1 echo'
EOF
eval $(minikube docker-env)
cd $TOPDIR/ && docker build -t khallware-echo:v1.0 .
cd - && rm -rf $TOPDIR

docker images |grep khallware-echo >/dev/null || echo 'BUILD DOCKER IMAGE FIRST'

## deploy the pod
kubectl run khallware --image=khallware-echo:v1.0 --port=4

## get status of the running docker containers
kubectl get deployments

## what is our pod name?
kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}'
export POD_NAME=$(!!)

kubectl expose deployment khallware --type=NodePort --name khallware-service
kubectl get services
kubectl describe services khallware-service
minikube service khallware-service --url

## connect and talk to the echo server
IPADDR=$(minikube ip)
PORT=$(minikube service khallware-service --url |cut -d/ -f3 |cut -d: -f2)
telnet $IPADDR $PORT # after each newline, should see text repeated

## connect to minikube (and info related to)
minikube ip
minikube ssh
minikube dashboard
minikube service list
minikube docker-env
kubectl get events
```

DESTROY
---------------
```shell
kubectl delete service khallware-service
kubectl delete deployment khallware
kubectl get deployments
minikube stop

### clean up any kubernetes ISO images that were downloaded
minikube delete
UUID=$(VBoxManage list vms |grep minikube |tail -1 |sed 's#^.*{\(.*\)}$#\1#')
#VBoxManage storagectl $UUID --name SATA --remove
VBoxManage unregistervm $UUID --delete
rm -rf $MINIKUBE_HOME ~/.minikube/
```
